package de.gameofstars.server;

import de.gameofstars.encryption.Hash;
import de.gameofstars.server.data.User;
import de.gameofstars.server.database.api.UserService;
import org.java_websocket.WebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

/**
 * Created by samuel on 28.02.15.
 */
@Component
public class UserManager {
    @Autowired
    private UserService database;

    public User login(String username, String password) {
        User user = database.findByName(username);
        try {
            String passwordHash = hashPassword(password);
            if (user != null && user.getPassword().equals(passwordHash)) {
                return user;
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String hashPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return Hash.generateMD5(Hash.generateSHA512(password));
    }

    public boolean usernameAvailable(String username) {
        return database.findByName(username) == null;
    }

    public void createUser(String username, String password) {
        try {
            User user = new User();
            user.setName(username);
            user.setPassword(hashPassword(password));
            database.insert(user);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}