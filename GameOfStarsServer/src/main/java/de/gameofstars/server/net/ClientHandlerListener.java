package de.gameofstars.server.net;

/**
 * Created by samuel on 01.03.15.
 */
public interface ClientHandlerListener {
    void onClientHandlerOpened(ClientHandler clientHandler);

    void onClientHandlerClosed(ClientHandler clientHandler);
}