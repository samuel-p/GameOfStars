package de.gameofstars.server.net;

import de.gameofstars.server.ConnectionManager;
import de.gameofstars.server.HighscoreManager;
import de.gameofstars.server.UserManager;
import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import de.gameofstars.net.RequestKeys;
import de.gameofstars.server.data.Highscore;
import de.gameofstars.server.data.User;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by samuel on 28.02.15.
 */
@Component
public class MessageManager implements MessageGenerator {

    private ResponseGenerator generator;

    @Autowired
    private ConnectionManager connectionManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private HighscoreManager highscoreManager;

    public MessageManager() {
        this.generator = new ResponseGenerator();
    }

    @Override
    public JSONObject generate(ClientHandler clientHandler, JSONObject request) {
        if (request.has(RequestKeys.ACTION)) {
            switch (request.getString(RequestKeys.ACTION)) {
                case RequestKeys.Action.LOGIN:
                    return actionLogin(clientHandler, request);
                case RequestKeys.Action.REGISTER:
                    return actionRegister(clientHandler, request);
                case RequestKeys.Action.COMMIT_SCORE:
                    return actionCommitScore(clientHandler, request);
                case RequestKeys.Action.LIST_SCORE:
                    return actionListScore(clientHandler, request);
            }
        }
        return generator.createErrorMessage(Messages.ACCESS_DENIED);
    }

    private JSONObject actionLogin(ClientHandler clientHandler, JSONObject request) {
        if (request.has(RequestKeys.User.NAME) && request.has(RequestKeys.User.PASSWORD)) {
            String username = request.getString(RequestKeys.User.NAME);
            String password = request.getString(RequestKeys.User.PASSWORD);
            return ckeckLogin(clientHandler, username, password);
        }
        return generator.createErrorMessage(Messages.ACCESS_DENIED);
    }

    private JSONObject actionRegister(ClientHandler clientHandler, JSONObject request) {
        if (request.has(RequestKeys.User.NAME) && request.has(RequestKeys.User.PASSWORD) && request.has(RequestKeys.User.PASSWORD2) && request.has(RequestKeys.User.PASSWORD_LENGTH)) {
            String username = request.getString(RequestKeys.User.NAME);
            String password = request.getString(RequestKeys.User.PASSWORD);
            String password2 = request.getString(RequestKeys.User.PASSWORD2);
            int passwordLength = request.getInt(RequestKeys.User.PASSWORD_LENGTH);
            if (!userManager.usernameAvailable(username)) {
                return generator.createErrorMessage(RequestKeys.Action.REGISTER, Messages.NAME_IN_USE);
            }
            if (passwordLength < 8) {
                return generator.createErrorMessage(RequestKeys.Action.REGISTER, Messages.SHORT_PASSWORD);
            }
            if (!password.equals(password2)) {
                return generator.createErrorMessage(RequestKeys.Action.REGISTER, Messages.PASSWORDS_DONT_MATCH);
            }
            userManager.createUser(username, password);
            return ckeckLogin(clientHandler, username, password);
        }
        return generator.createErrorMessage(Messages.ACCESS_DENIED);
    }

    private JSONObject ckeckLogin(ClientHandler clientHandler, String username, String password) {
        User user = userManager.login(username, password);
        if (user != null) {
            connectionManager.addConnection(user, clientHandler);
            return generator.createLoginMessage(user);
        }
        return generator.createErrorMessage(RequestKeys.Action.LOGIN, Messages.LOGIN_FAILED);
    }

    private JSONObject actionCommitScore(ClientHandler clientHandler, JSONObject request) {
        if (!request.has(RequestKeys.Highscore.SCORE)) {
            return generator.createErrorMessage(RequestKeys.Action.COMMIT_SCORE, Messages.DATA_MISSING);
        }
        if (!request.has(RequestKeys.Highscore.GAME_MODE)) {
            return generator.createErrorMessage(RequestKeys.Action.COMMIT_SCORE, Messages.DATA_MISSING);
        }
        if (!request.has(RequestKeys.Highscore.GAME_LEVEL)) {
            return generator.createErrorMessage(RequestKeys.Action.COMMIT_SCORE, Messages.DATA_MISSING);
        }
        if (!request.has(RequestKeys.Highscore.DATETIME)) {
            return generator.createErrorMessage(RequestKeys.Action.COMMIT_SCORE, Messages.DATA_MISSING);
        }
        int score = request.getInt(RequestKeys.Highscore.SCORE);
        User user = connectionManager.getUser(clientHandler);
        GameMode gameMode = GameMode.valueOf(request.getString(RequestKeys.Highscore.GAME_MODE));
        GameLevel gameLevel = GameLevel.valueOf(request.getString(RequestKeys.Highscore.GAME_LEVEL));
        Date date = new Date(request.getLong(RequestKeys.Highscore.DATETIME));
        highscoreManager.commitHighscore(score, user, gameMode, gameLevel, date);
        return generator.createSuccessMessage(RequestKeys.Action.COMMIT_SCORE, Messages.SUCCESS);
    }

    private JSONObject actionListScore(ClientHandler clientHandler, JSONObject request) {
        if (request.has(RequestKeys.Highscore.LIST_START_DATE) && request.has(RequestKeys.Highscore.GAME_MODE) && request.has(RequestKeys.Highscore.GAME_LEVEL) && request.has(RequestKeys.Highscore.LIST_START_INDEX) && request.has(RequestKeys.Highscore.LIST_SIZE)) {
            Date start = new Date(request.getLong(RequestKeys.Highscore.LIST_START_DATE));
            GameMode mode = GameMode.valueOf(request.getString(RequestKeys.Highscore.GAME_MODE));
            GameLevel level = GameLevel.valueOf(request.getString(RequestKeys.Highscore.GAME_LEVEL));
            int startIndex = request.getInt(RequestKeys.Highscore.LIST_START_INDEX);
            int listSize = request.getInt(RequestKeys.Highscore.LIST_SIZE);
            List<Highscore> highscores = highscoreManager.listHighscores(start, mode, level, startIndex, listSize);
            return generator.createHighscoresMessage(highscores);
        }
        return generator.createErrorMessage(RequestKeys.Action.LIST_SCORE, Messages.DATA_MISSING);
    }
}