package de.gameofstars.server.net;

/**
 * Created by samuel on 02.03.15.
 */
public class Messages {
    public static final String LOGIN_FAILED = "Login failed!";
    public static final String ACCESS_DENIED = "Access denied!";
    public static final String NAME_IN_USE = "This name is already in use!";
    public static final String SHORT_PASSWORD = "Your password is needs at least 8 characters!";
    public static final String PASSWORDS_DONT_MATCH = "The given passwords are not equal!";
    public static final String SUCCESS = "Success";
    public static final String DATA_MISSING = "Data missing!";
}