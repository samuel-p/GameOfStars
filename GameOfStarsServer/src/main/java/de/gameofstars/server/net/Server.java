package de.gameofstars.server.net;

import org.apache.log4j.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
import java.util.HashMap;

/**
 * Created by samuel on 27.02.15.
 */
@Component
public class Server extends WebSocketServer {
    private static final Logger logger = Logger.getLogger(Server.class);

    private HashMap<WebSocket, ClientHandler> clients;

    @Autowired
    private MessageGenerator generator;

    public Server(int port) {
        super(new InetSocketAddress(port));
        clients = new HashMap<WebSocket, ClientHandler>();
        start();
        logger.info("Server started on port " + port);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        if (!clients.containsKey(conn)) {
            ClientHandler client = new ClientHandler(conn);
            client.onOpened(handshake);
            clients.put(conn, client);
        }
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        if (clients.containsKey(conn)) {
            ClientHandler client = clients.get(conn);
            client.onClosed(code, reason, remote);
            clients.remove(conn);
        }
    }

    @Override
    public void onMessage(final WebSocket conn, String message) {
        if (clients.containsKey(conn)) {
            ClientHandler client = clients.get(conn);
            client.onMessage(message, generator);
            clients.put(conn, client);
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        logger.error("An error occurred in the Server!", ex);
    }
}