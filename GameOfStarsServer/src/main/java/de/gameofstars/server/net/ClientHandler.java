package de.gameofstars.server.net;

import de.gameofstars.encryption.Encryption;
import org.apache.log4j.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samuel on 02.03.15.
 */
public class ClientHandler {
    private static final Logger logger = Logger.getLogger(ClientHandler.class);

    private WebSocket webSocket;
    private List<ClientHandlerListener> listeners;

    public ClientHandler(WebSocket webSocket) {
        this.webSocket = webSocket;
        listeners = new ArrayList<ClientHandlerListener>();
    }

    public void addClientHandlerListener(ClientHandlerListener listener) {
        listeners.add(listener);
    }

    public void removeClientHandlerListener(ClientHandlerListener listener) {
        listeners.remove(listener);
    }

    public void send(JSONObject message) {
        if (message != null) {
            try {
                String encrypted = Encryption.encrypt(message.toString());
                webSocket.send(encrypted);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onOpened(ClientHandshake handshake) {
        for (ClientHandlerListener listener : listeners) {
            listener.onClientHandlerOpened(this);
        }
    }

    public void onMessage(String message, MessageGenerator generator) {
        try {
            String decrypted = Encryption.decrypt(message);
            JSONObject request = new JSONObject(decrypted);
            logger.info("Request: " + request);
            JSONObject response = generator.generate(this, request);
            logger.info("Response: " + response);
            send(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClosed(int code, String reason, boolean remote) {
        for (ClientHandlerListener listener : listeners) {
            listener.onClientHandlerClosed(this);
        }
    }
}