package de.gameofstars.server.net;

import org.json.JSONObject;

/**
 * Created by samuel on 28.02.15.
 */
public interface MessageGenerator {
    JSONObject generate(ClientHandler clientHandler, JSONObject request);
}