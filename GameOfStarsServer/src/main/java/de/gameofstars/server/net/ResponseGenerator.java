package de.gameofstars.server.net;

import de.gameofstars.server.data.Highscore;
import de.gameofstars.server.data.User;
import de.gameofstars.net.RequestKeys;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by samuel on 01.03.15.
 */
public class ResponseGenerator {
    private JSONObject createBasicMessage(boolean state, String action) {
        JSONObject message = new JSONObject();
        message.put(RequestKeys.STATE, state);
        if (action != null) {
            message.put(RequestKeys.ACTION, action);
        }
        return message;
    }

    public JSONObject createErrorMessage(String action, String errorMessage) {
        JSONObject message = createBasicMessage(false, action);
        if (errorMessage != null) {
            message.put(RequestKeys.MESSAGE, errorMessage);
        }
        return message;
    }

    public JSONObject createErrorMessage(String message) {
        return createErrorMessage(null, message);
    }

    public JSONObject createSuccessMessage(String action, String successMessage) {
        JSONObject message = createBasicMessage(true, action);
        if (successMessage != null) {
            message.put(RequestKeys.MESSAGE, successMessage);
        }
        return message;
    }

    public JSONObject createLoginMessage(User user) {
        JSONObject message = createBasicMessage(true, RequestKeys.Action.LOGIN);
        message.put(RequestKeys.User.ID, user.getId());
        message.put(RequestKeys.User.NAME, user.getName());
        return message;
    }

    public JSONObject createHighscoresMessage(List<Highscore> highscores) {
        JSONObject message = createBasicMessage(true, RequestKeys.Action.LIST_SCORE);
        JSONArray scoreList = new JSONArray();
        int position = 0;
        for (Highscore highscore : highscores) {
            JSONObject score = new JSONObject();
            if (position == 0 || highscore.getScore() != highscores.get(position - 1).getScore()) {
                position++;
            }
            score.put(RequestKeys.Highscore.POSITION, position);
            score.put(RequestKeys.User.NAME, highscore.getUser().getName());
            score.put(RequestKeys.Highscore.SCORE, highscore.getScore());
            scoreList.put(score);
        }
        message.put(RequestKeys.Highscore.LIST, scoreList);
        return message;
    }
}