package de.gameofstars.server.data;

import javax.persistence.*;

/**
 * Created by samuel on 27.02.15.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = User.ALL, query = "from User"),
        @NamedQuery(name = User.NAME, query = "from User u where u.name like :username")
})
public class User {
    public static final String ALL = "User.ALL";
    public static final String NAME = "User.NAME";
    public static final String USERNAME = "username";

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private String name;

    private String password;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}