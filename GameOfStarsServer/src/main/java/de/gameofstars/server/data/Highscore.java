package de.gameofstars.server.data;

import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by samuel on 28.02.15.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Highscore.SORTED, query = "from Highscore h where h.date between :startDate and :now and h.gameMode = :gameMode and h.gameLevel = :gameLevel order by h.score desc"),
        @NamedQuery(name = Highscore.USER, query = "from Highscore h where h.user.id = :userid order by h.score desc")
})
public class Highscore {
    public static final String SORTED = "Highscore.SORTED";
    public static final String USER = "Highscore.USER";

    public static final String USERID = "userid";
    public static final String GAME_LEVEL = "gameLevel";
    public static final String GAME_MODE = "gameMode";
    public static final String NOW = "now";
    public static final String START_DATE = "startDate";

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private User user;

    private int score;

    @DateTimeFormat
    private Date date;

    private int gameMode;

    private int gameLevel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public GameMode getGameMode() {
        return GameMode.values()[gameMode];
    }

    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode.ordinal();
    }

    public GameLevel getGameLevel() {
        return GameLevel.values()[gameLevel];
    }

    public void setGameLevel(GameLevel gameLevel) {
        this.gameLevel = gameLevel.ordinal();
    }
}