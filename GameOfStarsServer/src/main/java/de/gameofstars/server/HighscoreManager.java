package de.gameofstars.server;

import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import de.gameofstars.server.data.Highscore;
import de.gameofstars.server.data.User;
import de.gameofstars.server.database.api.HighscoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by samuel on 03.03.15.
 */
@Component
public class HighscoreManager {
    @Autowired
    private HighscoreService database;

    public void commitHighscore(int score, User user, GameMode gameMode, GameLevel gameLevel, Date date) {
        Highscore highscore = new Highscore();
        highscore.setScore(score);
        highscore.setUser(user);
        highscore.setGameMode(gameMode);
        highscore.setGameLevel(gameLevel);
        highscore.setDate(date);
        database.insert(highscore);
    }

    public List<Highscore> listHighscores(Date start, GameMode mode, GameLevel level, int startIndex, int listSize) {
        // TODO ...
        return database.find(start, mode, level, startIndex, listSize);
    }
}