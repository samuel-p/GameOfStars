package de.gameofstars.server;

import de.gameofstars.server.data.User;
import de.gameofstars.server.net.ClientHandler;
import de.gameofstars.server.net.ClientHandlerListener;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samuel on 02.03.15.
 */
@Component
public class ConnectionManager implements ClientHandlerListener {
    private BidiMap<User, ClientHandler> managingList;

    public ConnectionManager() {
        managingList = new DualHashBidiMap<User, ClientHandler>();
    }

    public void addConnection(User user, ClientHandler client) {
        client.addClientHandlerListener(this);
        managingList.put(user, client);
    }

    public User getUser(ClientHandler clientHandler) {
        return managingList.getKey(clientHandler);
    }

    public List<User> getOnlineUsers() {
        return new ArrayList<User>(managingList.keySet());
    }

    @Override
    public void onClientHandlerOpened(ClientHandler clientHandler) {
    }

    @Override
    public void onClientHandlerClosed(ClientHandler clientHandler) {
        managingList.removeValue(clientHandler);
    }
}