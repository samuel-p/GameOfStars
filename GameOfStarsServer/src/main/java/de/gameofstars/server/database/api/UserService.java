package de.gameofstars.server.database.api;

import de.gameofstars.server.data.User;

import java.util.List;

/**
 * Created by samuel on 02.03.15.
 */
public interface UserService {
    public void insert(User user);

    public void update(User user);

    public void delete(User user);

    public User findById(long id);

    public List<User> find();

    public User findByName(String username);
}