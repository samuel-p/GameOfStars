package de.gameofstars.server.database.impl;

import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import de.gameofstars.server.data.Highscore;
import de.gameofstars.server.data.User;
import de.gameofstars.server.database.api.DatabaseService;
import de.gameofstars.server.database.api.HighscoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by samuel on 02.03.15.
 */
@Component
public class HighscoreHandler implements HighscoreService {

    @Autowired
    private DatabaseService database;

    @Override
    public void insert(Highscore highscore) {
        database.insert(highscore);
    }

    @Override
    public List<Highscore> find(Date start, GameMode mode, GameLevel level, int startIndex, int maxCount) {
        Query query = database.createQuery(Highscore.SORTED, Highscore.class);
        query.setParameter(Highscore.START_DATE, start);
        query.setParameter(Highscore.NOW, new Date(System.currentTimeMillis()));
        query.setParameter(Highscore.GAME_MODE, mode.ordinal());
        query.setParameter(Highscore.GAME_LEVEL, level.ordinal());
        if (startIndex > 0) {
            query.setFirstResult(startIndex);
        }
        if (maxCount > 0) {
            query.setMaxResults(maxCount);
        }
        return database.findWithQuery(query);
    }

    @Override
    public List<Highscore> findUserResults(User user) {
        Query query = database.createQuery(Highscore.USER, Highscore.class);
        query.setParameter(Highscore.USERID, user.getId());
        return database.findWithQuery(query);
    }
}