package de.gameofstars.server.database.impl;

import de.gameofstars.server.data.User;
import de.gameofstars.server.database.api.DatabaseService;
import de.gameofstars.server.database.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by samuel on 02.03.15.
 */
@Component
public class UserHandler implements UserService {
    @Autowired
    private DatabaseService database;

    @Override
    public void insert(User user) {
        database.insert(user);
    }

    @Override
    public void update(User user) {
        database.update(user);
    }

    @Override
    public void delete(User user) {
        database.delete(user);
    }

    @Override
    public User findById(long id) {
        return database.find(User.class, id);
    }

    @Override
    public List<User> find() {
        return database.findWithQuery(User.ALL, User.class);
    }

    @Override
    public User findByName(String username) {
        Query query = database.createQuery(User.NAME, User.class);
        query.setParameter(User.USERNAME, username);
        List<User> users = database.findWithQuery(query);
        if (users.size() == 1) {
            return users.get(0);
        }
        return null;
    }
}