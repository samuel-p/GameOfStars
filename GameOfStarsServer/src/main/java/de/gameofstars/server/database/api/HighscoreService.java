package de.gameofstars.server.database.api;

import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import de.gameofstars.server.data.Highscore;
import de.gameofstars.server.data.User;

import java.util.Date;
import java.util.List;

/**
 * Created by samuel on 02.03.15.
 */
public interface HighscoreService {
    void insert(Highscore highscore);

    List<Highscore> find(Date start, GameMode mode, GameLevel level, int startIndex, int maxCount);

    List<Highscore> findUserResults(User user);
}