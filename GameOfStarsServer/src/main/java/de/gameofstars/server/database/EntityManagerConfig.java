package de.gameofstars.server.database;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by samuel on 02.03.15.
 */
@Configuration
public class EntityManagerConfig {
    private EntityManagerFactory emf;

    public EntityManagerConfig(String persistenceUnit) {
        emf = Persistence.createEntityManagerFactory(persistenceUnit);
    }

    @Bean
    public EntityManager entityManager() {
        return emf.createEntityManager();
    }
}