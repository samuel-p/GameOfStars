package de.gameofstars.server.database.api;

import de.gameofstars.server.data.User;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by samuel on 02.03.15.
 */
public interface DatabaseService {
    void update(Object entity);

    void insert(Object entity);

    void delete(Object entity);

    <T> T find(Class<T> clazz, Object id);

    <T> List<T> findWithQuery(String query, Class<T> clazz);

    Query createQuery(String query, Class clazz);

    <T> List<T> findWithQuery(Query query);
}