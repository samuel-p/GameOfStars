package de.gameofstars.server;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ServerMain {
    public static final String CONFIG_LOCATION = "servlet_context.xml";

    public static void main(String[] args) throws Exception {
        new ClassPathXmlApplicationContext(CONFIG_LOCATION);
    }
}