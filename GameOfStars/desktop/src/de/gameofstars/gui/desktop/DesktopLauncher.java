package de.gameofstars.gui.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.gameofstars.gui.GameOfStars;
import org.lwjgl.opengl.Display;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 550;
        config.height = 700;
        config.addIcon("imgs/launcher-16.png", Files.FileType.Internal);
        config.addIcon("imgs/launcher-32.png", Files.FileType.Internal);
        config.addIcon("imgs/launcher-128.png", Files.FileType.Internal);
        new LwjglApplication(new GameOfStars(), config);
    }
}