package de.gameofstars.logic;

import com.badlogic.gdx.utils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Counter extends Timer.Task implements BoardListener, GameListener {
    private List<CounterListener> listeners;
    private int points;
    private GameMode mode;
    private int seconds;
    private Timer timer;
    private int moves;

    public Counter(Game game) {
        this.listeners = new ArrayList<CounterListener>();
        game.addGameListener(this);
        game.getBoard().addBoardListener(this);
        timer = new Timer();
    }

    public void addCounterListener(CounterListener listener) {
        listeners.add(listener);
    }

    public void removeCounterListener(CounterListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void onFieldsCleared(Field[] clearedFields, int[] clearedGroupSizes) {
        for (int groupSize : clearedGroupSizes) {
            points += groupSize * 10;
        }
        invokePointsChanged();
    }

    private void invokePointsChanged() {
        for (CounterListener listener : listeners) {
            listener.onPointsChanged(points);
        }
    }

    @Override
    public void onFieldsSwapped(Field field1, Field field2) {
        if (mode != GameMode.SINGLE_PLAYER_NORMAL) {
            return;
        }
        moves--;
        invokeMovesChanged();
    }

    private void invokeMovesChanged() {
        for (CounterListener listener : listeners) {
            listener.onMovesChanged(moves);
        }
    }

    @Override
    public void onFieldsNotSwapped(Field field1, Field field2) {
        if (mode != GameMode.SINGLE_PLAYER_NORMAL) {
            return;
        }
        moves++;
        invokeMovesChanged();
    }

    @Override
    public void onShuffled() {
    }

    @Override
    public void onFieldsMovedDown(Map<Field, Integer> fields) {
    }

    @Override
    public void onGameStarted(GameMode mode, GameLevel level) {
        this.mode = mode;
        points = 0;
        invokePointsChanged();
        if (mode == GameMode.SINGLE_PLAYER_NORMAL) {
            switch (level) {
                case EASY:
                    moves = 40;
                    break;
                case NORMAL:
                    moves = 30;
                    break;
                case HARD:
                    moves = 20;
                    break;
            }
            invokeMovesChanged();
        } else if (mode == GameMode.SINGLE_PLAYER_TIME) {
            switch (level) {
                case EASY:
                    seconds = 100;
                    break;
                case NORMAL:
                    seconds = 80;
                    break;
                case HARD:
                    seconds = 60;
                    break;
            }
            invokeTimeChanged();
            timer.scheduleTask(this, 1, 1);
            timer.start();
        }
    }

    @Override
    public void onGameEnd(GameMode mode, GameLevel level) {
    }

    public int getPoints() {
        return points;
    }

    @Override
    public void run() {
        seconds--;
        if (seconds == 0) {
            timer.stop();
            timer.clear();
        }
        invokeTimeChanged();
    }

    private void invokeTimeChanged() {
        for (CounterListener listener : listeners) {
            listener.onTimeChanged(seconds);
        }
    }
}