package de.gameofstars.logic;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by xce35l4 on 17.02.2015.
 */
public enum FieldColor {
    RED(Color.RED), ORANGE(new Color(0xff9400ff)), YELLOW(Color.YELLOW), GREEN(Color.GREEN), BLUE(Color.BLUE), PURPLE(Color.PURPLE), GREY(Color.GRAY);

    private Color color;

    private FieldColor(Color color) {
        this.color = color;
    }

    public boolean equals(FieldColor color) {
        return this == color;
    }

    public Color getColor() {
        return color;
    }

}