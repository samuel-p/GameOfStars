package de.gameofstars.logic;

public interface CounterListener {
	void onMovesChanged(int movesLeft);

    void onTimeChanged(int secondsLeft);

	void onPointsChanged(int points);
}