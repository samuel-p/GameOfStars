package de.gameofstars.logic;

import de.gameofstars.gui.HighscoreScreenListener;

import java.util.ArrayList;
import java.util.List;

public class Game implements CounterListener {
    private List<GameListener> listeners;
    private Counter counter;
    private Board board;
    private GameMode mode;
    private GameLevel level;

    public Game() {
        listeners = new ArrayList<GameListener>();
		board = new Board();
        counter = new Counter(this);
        counter.addCounterListener(this);
    }

    public void start(GameMode mode, GameLevel level) {
        this.mode = mode;
        this.level = level;
        board.shuffle();
        for (GameListener listener : listeners) {
            listener.onGameStarted(mode, level);
        }
    }

    public void addGameListener(GameListener listener) {
        listeners.add(listener);
    }

    public void removeGameListener(GameListener listener) {
        listeners.remove(listener);
    }

    public Counter getCounter() {
        return counter;
    }

    public Board getBoard() {
        return board;
    }

    private void invokeGameEnd() {
        for (GameListener listener : listeners) {
            listener.onGameEnd(mode, level);
        }
    }

    @Override
    public void onMovesChanged(int movesLeft) {
        if (movesLeft == 0) {
            invokeGameEnd();
        }
    }

    @Override
    public void onTimeChanged(int secondsLeft) {
        if (secondsLeft == 0) {
            invokeGameEnd();
        }
    }

    @Override
    public void onPointsChanged(int points) {

    }

    public GameMode getMode() {
        return mode;
    }

    public GameLevel getLevel() {
        return level;
    }
}