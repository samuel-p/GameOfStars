package de.gameofstars.logic;

import java.util.Random;
import java.util.Set;

public class FieldGenerator {
    private Random r;
    private FieldColor[] colorValues;

    public FieldGenerator() {
        r = new Random();
        colorValues = FieldColor.values();
    }

    public FieldColor nextRandomColor() {
        FieldColor color;
        int next = r.nextInt(colorValues.length);
        color = colorValues[next];
        return color;
    }

    public FieldColor nextRandomColor(Set<FieldColor> excludedColors) {
        if (excludedColors.size() >= colorValues.length) {
            throw new IllegalArgumentException("No color is valid with that exclusion");
        }
        FieldColor color;
        do {
            color = nextRandomColor();
        } while (excludedColors.contains(color));
        return color;
    }

    public int nextRandomArrayPadding(int arrayLength) {
        return r.nextInt(arrayLength);
    }
}