package de.gameofstars.logic;

import java.util.*;

/**
 * Created by xce35l4 on 17.02.2015.
 */
public class Board {
    public static int WIDTH = 9;
    public static int HEIGHT = 9;

    private Field[][] board;
    private FieldGenerator generator;
    private List<BoardListener> listeners;

    public Board() {
        generator = new FieldGenerator();
        listeners = new ArrayList<BoardListener>();
        initFields();
        initNextFields();
    }

    private void initFields() {
        board = new Field[HEIGHT][WIDTH];
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                board[y][x] = new Field(x, y);
            }
        }
    }

    private void initNextFields() {
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                if (x > 0) {
                    board[y][x].setLeft(board[y][x - 1]);
                }
                if (x < WIDTH - 1) {
                    board[y][x].setRight(board[y][x + 1]);
                }
                if (y > 0) {
                    board[y][x].setTop(board[y - 1][x]);
                }
                if (y < HEIGHT - 1) {
                    board[y][x].setBottom(board[y + 1][x]);
                }
            }
        }
    }

    public void addBoardListener(BoardListener listener) {
        listeners.add(listener);
    }

    public void removeBoardListener(BoardListener listener) {
        listeners.remove(listener);
    }

    public void shuffle() {
        Set<FieldColor> excluded = new HashSet<FieldColor>();
        do {
            for (Field field : getFields()) {
                if (field.getX() >= 2
                        && field.getLeft().getColor().equals(field.getLeft().getLeft()
                        .getColor())) {
                    excluded.add(field.getLeft().getColor());
                }
                if (field.getY() >= 2
                        && field.getTop().getColor().equals(field.getTop().getTop()
                        .getColor())) {
                    excluded.add(field.getTop().getColor());
                }
                field.setColor(generator.nextRandomColor(excluded));
                excluded.clear();
            }
        } while (searchPossibleSwitches().length == 0);
        for (BoardListener listener : listeners) {
            listener.onShuffled();
        }
    }

    public Field[] getFields() {
        List<Field> fields = new ArrayList<Field>();
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                fields.add(board[y][x]);
            }
        }
        return parseFieldList(fields);
    }

    public void swapFields(Field field1, Field field2) {
        if (field1 != null && !field1.isNext(field2) || field1 == field2) {
            System.err.println("not permitted");
            return;
        }
        swapFieldColors(field1, field2);
        for (BoardListener listener : listeners) {
            listener.onFieldsSwapped(field1, field2);
        }
        List<Field> clearedFields = new ArrayList<Field>();
        List<Integer> groupSizes = new ArrayList<Integer>();
        clearSameFields(clearedFields, groupSizes);
        if (clearedFields.isEmpty()) {
            System.err.println("not switched");
            swapFieldColors(field1, field2);
            for (BoardListener listener : listeners) {
                listener.onFieldsNotSwapped(field1, field2);
            }
            return;
        }
        Field[] clearedFieldsArray = parseFieldList(clearedFields);
        int[] groupSizesArray = new int[groupSizes.size()];
        for (int i = 0; i < groupSizesArray.length; i++) {
            groupSizesArray[i] = groupSizes.get(i);
        }
        for (BoardListener listener : listeners) {
            listener.onFieldsCleared(clearedFieldsArray, groupSizesArray);
        }
    }

    public boolean clearFields() {
        List<Field> clearedFields = new ArrayList<Field>();
        List<Integer> groupSizes = new ArrayList<Integer>();
        clearSameFields(clearedFields, groupSizes);
        if (!clearedFields.isEmpty()) {
            Field[] clearedFieldsArray = parseFieldList(clearedFields);
            int[] groupSizesArray = new int[groupSizes.size()];
            for (int i = 0; i < groupSizesArray.length; i++) {
                groupSizesArray[i] = groupSizes.get(i);
            }
            for (BoardListener listener : listeners) {
                listener.onFieldsCleared(clearedFieldsArray, groupSizesArray);
            }
            return true;
        }
        if (searchPossibleSwitches().length == 0) {
            shuffle();
        }
        return false;
    }

    public void moveFields() {
        Map<Field, Integer> movedFields = new HashMap<Field, Integer>();
        moveFieldColorsDown(movedFields);
        for (BoardListener listener : listeners) {
            listener.onFieldsMovedDown(movedFields);
        }
    }

    public Field getField(int x, int y) {
        return board[y][x];
    }

    public Field[] searchPossibleSwitches() {
        List<Field> possibleFields = new ArrayList<Field>();
        Field[] allFields = getFields();
        int padding = generator.nextRandomArrayPadding(allFields.length);
        for (int i = 0; i < allFields.length; i++) {
            Field field = allFields[(padding + i) % allFields.length];
            if (checkLeftRightBottom(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkLeftRightTop(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkRightLeftBottom(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkRightLeftTop(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkTopBottomLeft(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkTopBottomRight(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkBottomTopLeft(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkBottomTopRight(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkLeftBottomLeftTop(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkRightBottomRightTop(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkBottomLeftBottomRight(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkTopLeftRopRight(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkLeftTwiceRight(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkRightTwiceLeft(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkBottomTwiceTop(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
            if (checkTopTwiceBottom(field, possibleFields)) {
                return parseFieldList(possibleFields);
            }
        }
        return parseFieldList(possibleFields);
    }

    private void moveFieldColorsDown(Map<Field, Integer> fields) {
        for (int x = 0; x < WIDTH; x++) {
            for (int y = HEIGHT - 1; y >= 0; y--) {
                Field field = board[y][x];
                int fieldsMoved = moveFieldColorDown(field);
                if (fieldsMoved > 0) {
                    fields.put(getField(x, y + fieldsMoved), fieldsMoved);
                }
            }
        }
        int[] columnEmptyFields = new int[WIDTH];
        for (int x = 0; x < WIDTH; x++) {
            for (int y = HEIGHT - 1; y >= 0; y--) {
                if (board[y][x].getColor() == null) {
                    columnEmptyFields[x]++;
                }
            }
        }
        for (int x = 0; x < WIDTH; x++) {
            for (int y = HEIGHT - 1; y >= 0; y--) {
                Field field = board[y][x];
                if (field.getColor() == null) {
                    field.setColor(generator.nextRandomColor());
                    fields.put(field, columnEmptyFields[field.getX()]);
                }
            }
        }
    }

    private int moveFieldColorDown(Field field) {
        if (field.getColor() != null && field.getBottom() != null && field.getBottom().getColor() == null) {
            swapFieldColors(field, field.getBottom());
            return moveFieldColorDown(field.getBottom()) + 1;
        }
        return 0;
    }

    private void swapFieldColors(Field field1, Field field2) {
        if (field1 == null || field2 == null) {
            return;
        }
        FieldColor color = field1.getColor();
        field1.setColor(field2.getColor());
        field2.setColor(color);
    }

    private void clearSameFields(List<Field> clearedFields, List<Integer> groupSizes) {
        findHorizontalSameFields(clearedFields, groupSizes);
        findVerticalSameFields(clearedFields, groupSizes);
        for (Field field : clearedFields) {
            field.setColor(null);
        }
    }

    private boolean isEdge(Field field, List<Field> clearedFields) {
        if (sameColors(field, field.getLeft(), field.getTop()) && clearedFields.contains(field.getLeft()) && clearedFields.contains(field.getTop())) {
            return true;
        }
        if (sameColors(field, field.getTop(), field.getRight()) && clearedFields.contains(field.getTop()) && clearedFields.contains(field.getRight())) {
            return true;
        }
        if (sameColors(field, field.getRight(), field.getBottom()) && clearedFields.contains(field.getRight()) && clearedFields.contains(field.getBottom())) {
            return true;
        }
        if (sameColors(field, field.getBottom(), field.getLeft()) && clearedFields.contains(field.getBottom()) && clearedFields.contains(field.getLeft())) {
            return true;
        }
        return false;
    }

    private boolean sameColors(Field field, Field next1, Field next2) {
        if (field == null || next1 == null || next2 == null) {
            return false;
        }
        if (field.getColor().equals(next1.getColor()) && field.getColor().equals(next2.getColor())) {
            return true;
        }
        return false;
    }

    private void findHorizontalSameFields(List<Field> clearedFields,
                                          List<Integer> groupSizes) {
        for (int y = 0; y < HEIGHT; y++) {
            Set<Field> sameColorFields = new HashSet<Field>();
            FieldColor sameColor = board[y][0].getColor();
            int sameColorsCount = 1;
            sameColorFields.add(board[y][0]);
            for (int x = 1; x < WIDTH; x++) {
                Field current = board[y][x];
                if (current.getColor().equals(sameColor)) {
                    sameColorsCount++;
                    sameColorFields.add(current);
                }
                if (!current.getColor().equals(sameColor) || x == WIDTH - 1) {
                    if (sameColorsCount >= 3) {
                        clearedFields.addAll(sameColorFields);
                        groupSizes.add(sameColorsCount);
                    }
                    sameColorFields.clear();
                    sameColorFields.add(current);
                    sameColor = current.getColor();
                    sameColorsCount = 1;
                }
            }
        }
    }

    private void findVerticalSameFields(List<Field> clearedFields,
                                        List<Integer> groupSizes) {
        for (int x = 0; x < WIDTH; x++) {
            Set<Field> sameColorFields = new HashSet<Field>();
            FieldColor sameColor = board[0][x].getColor();
            int sameColorsCount = 1;
            sameColorFields.add(board[0][x]);
            for (int y = 1; y < HEIGHT; y++) {
                Field current = board[y][x];
                if (current.getColor().equals(sameColor)) {
                    sameColorsCount++;
                    sameColorFields.add(current);
                }
                if (!current.getColor().equals(sameColor) || y == HEIGHT - 1) {
                    if (sameColorsCount >= 3) {
                        clearedFields.addAll(sameColorFields);
                        groupSizes.add(sameColorsCount);
                    }
                    sameColorFields.clear();
                    sameColorFields.add(current);
                    sameColor = current.getColor();
                    sameColorsCount = 1;
                }
            }
        }
    }

    private Field[] parseFieldList(List<Field> fields) {
        return fields.toArray(new Field[fields.size()]);
    }

    private boolean checkLeftRightBottom(Field field, List<Field> possibleFields) {
        if (field.getRight() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getLeft(), field.getRight().getBottom(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkLeftRightTop(Field field, List<Field> possibleFields) {
        if (field.getRight() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getLeft(), field.getRight().getTop(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkRightLeftBottom(Field field, List<Field> possibleFields) {
        if (field.getLeft() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getRight(), field.getLeft().getBottom(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkRightLeftTop(Field field, List<Field> possibleFields) {
        if (field.getLeft() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getRight(), field.getLeft().getTop(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkTopBottomLeft(Field field, List<Field> possibleFields) {
        if (field.getBottom() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getTop(), field.getBottom().getLeft(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkTopBottomRight(Field field, List<Field> possibleFields) {
        if (field.getBottom() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getTop(), field.getBottom().getRight(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkBottomTopLeft(Field field, List<Field> possibleFields) {
        if (field.getTop() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getBottom(), field.getTop().getLeft(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkBottomTopRight(Field field, List<Field> possibleFields) {
        if (field.getTop() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getBottom(), field.getTop().getRight(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkLeftBottomLeftTop(Field field, List<Field> possibleFields) {
        if (field.getLeft() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getLeft().getBottom(), field.getLeft().getTop(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkRightBottomRightTop(Field field, List<Field> possibleFields) {
        if (field.getRight() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getRight().getBottom(), field.getRight().getTop(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkBottomLeftBottomRight(Field field, List<Field> possibleFields) {
        if (field.getBottom() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getBottom().getLeft(), field.getBottom().getRight(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkTopLeftRopRight(Field field, List<Field> possibleFields) {
        if (field.getTop() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getTop().getLeft(), field.getTop().getRight(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkLeftTwiceRight(Field field, List<Field> possibleFields) {
        if (field.getRight() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getLeft(), field.getRight().getRight(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkRightTwiceLeft(Field field, List<Field> possibleFields) {
        if (field.getLeft() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getRight(), field.getLeft().getLeft(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkBottomTwiceTop(Field field, List<Field> possibleFields) {
        if (field.getTop() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getBottom(), field.getTop().getTop(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkTopTwiceBottom(Field field, List<Field> possibleFields) {
        if (field.getBottom() == null) {
            return false;
        }
        if (checkFieldsForColorAddIfSame(field, field.getTop(), field.getBottom().getBottom(), possibleFields)) {
            return true;
        }
        return false;
    }

    private boolean checkFieldsForColorAddIfSame(Field field1, Field field2, Field field3, List<Field> possibleFields) {
        if (sameColors(field1, field2, field3)) {
            possibleFields.add(field1);
            possibleFields.add(field2);
            possibleFields.add(field3);
            return true;
        }
        return false;
    }
}