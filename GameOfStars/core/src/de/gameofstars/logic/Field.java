package de.gameofstars.logic;
/**
 * Created by xce35l4 on 17.02.2015.
 */
public class Field {
    private int x;
    private int y;
    private FieldColor color;
    private Field left;
    private Field top;
    private Field right;
    private Field bottom;

    public Field(int x, int y) {
        this(x, y, null);
    }

    public Field(int x, int y, FieldColor color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public FieldColor getColor() {
        return color;
    }

    public void setColor(FieldColor color) {
        this.color = color;
    }

	public Field getLeft() {
		return left;
	}

	public void setLeft(Field left) {
		this.left = left;
	}

	public Field getTop() {
		return top;
	}

	public void setTop(Field top) {
		this.top = top;
	}

	public Field getRight() {
		return right;
	}

	public void setRight(Field right) {
		this.right = right;
	}

	public Field getBottom() {
		return bottom;
	}

	public void setBottom(Field bottom) {
		this.bottom = bottom;
	}

	public boolean isNext(Field field) {
		if (left != null && left == field) {
			return true;
		}
		if (top != null && top == field) {
			return true;
		}
		if (right != null && right == field) {
			return true;
		}
		if (bottom != null && bottom == field) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Field [x=" + x + ", y=" + y + ", color=" + color + "]";
	}
}