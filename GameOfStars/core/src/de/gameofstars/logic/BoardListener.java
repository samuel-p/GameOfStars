package de.gameofstars.logic;


import java.util.Map;

public interface BoardListener {
    void onFieldsSwapped(Field field1, Field field2);

    void onFieldsNotSwapped(Field field1, Field field2);

	void onShuffled();

    void onFieldsCleared(Field[] clearedFields, int[] clearedGroupSizes);

	void onFieldsMovedDown(Map<Field, Integer> fields);
}