package de.gameofstars.logic;

/**
 * Created by samuel on 25.02.15.
 */
public interface GameListener {
    void onGameStarted(GameMode mode, GameLevel level);

    void onGameEnd(GameMode mode, GameLevel level);
}