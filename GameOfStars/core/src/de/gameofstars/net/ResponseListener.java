package de.gameofstars.net;

import de.gameofstars.gui.HighscoreListItem;

import java.util.List;

/**
 * Created by samuel on 28.02.15.
 */
public interface ResponseListener {
    void onLoggedIn(String username);

    void onLogginError(String message);

    void onRegisterError(String message);

    void onListScore(List<HighscoreListItem> highscores);
}