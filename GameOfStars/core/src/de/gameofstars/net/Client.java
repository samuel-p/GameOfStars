package de.gameofstars.net;

import de.gameofstars.encryption.Encryption;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by samuel on 27.02.15.
 */
public class Client extends WebSocketClient {
    private ClientListener listener;
    private ResponseHandler handler;
    private boolean connected;

    public Client(String host, int port, ClientListener listener, ResponseHandler handler) {
        super(generateURI(host, port));
        this.listener = listener;
        this.handler = handler;
    }

    private static URI generateURI(String host, int port) {
        try {
            String address = "ws://" + host + ":" + port + "/";
            System.out.println(address);
            return new URI(address);
        } catch (URISyntaxException e) {
            return null;
        }
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        connected = true;
        if (listener != null) {
            listener.onClientOpened();
        }
    }

    public void send(JSONObject message) {
        if (message != null) {
            try {
                String encrypted = Encryption.encrypt(message.toString());
                send(encrypted);
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onMessage(String message) {
        try {
            String decrypted = Encryption.decrypt(message);
            JSONObject response = new JSONObject(decrypted);
            handler.handleResponse(response);
        } catch (Exception e) {
        }
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        connected = false;
        if (listener != null) {
            listener.onClientClosed();
        }
    }

    @Override
    public void onError(Exception ex) {
    }

    public boolean isConnected() {
        return connected;
    }
}