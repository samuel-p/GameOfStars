package de.gameofstars.net;

import org.json.JSONObject;

/**
 * Created by samuel on 28.02.15.
 */
public interface ResponseHandler {
    void handleResponse(JSONObject response);
}