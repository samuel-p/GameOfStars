package de.gameofstars.net;

import de.gameofstars.data.Files;
import de.gameofstars.data.JsonArrayStorage;
import de.gameofstars.data.Storage;
import de.gameofstars.encryption.Hash;
import de.gameofstars.gui.HighscoreListItem;
import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

/**
 * Created by samuel on 01.03.15.
 */
public class ClientManager implements ClientListener, ResponseListener {
    private static final String HOST = /*"srv2.sp-codes.de";//*/"localhost";
    private static final int PORT = 8080;

    public static final String CACHE_NAME = "cache";
    public static final int RECONNECT_DELAY = 1000;

    private ResponseManager handler;
    private RequestGenerator requestGenerator;
    private JsonArrayStorage cache;
    private Client client;
    private JSONObject openingRequest;
    private Storage storage;
    private ResponseListener listener;

    public ClientManager(Storage storage, ResponseListener listener) {
        this.listener = listener;
        this.requestGenerator = new RequestGenerator();
        this.storage = storage;
        this.handler = new ResponseManager(this);
        this.client = new Client(HOST, PORT, this, handler);
        File file = Files.getFile(CACHE_NAME);
        this.cache = new JsonArrayStorage(file);
        if (storage.isUserLoggedIn()) {
            login(storage.getUsername(), storage.getPassword());
        }
    }

    private void open() {
        if (!client.isConnected()) {
            client.connect();
        }
    }

    @Override
    public void onClientOpened() {
        System.err.println("Opened client");
        if (openingRequest != null) {
            client.send(openingRequest);
        }
        for (JSONObject request : cache.get()) {
            client.send(request);
        }
        cache.clear();
    }

    @Override
    public void onClientClosed() {
        System.out.println("close");
        try {
            Thread.sleep(RECONNECT_DELAY);
        } catch (InterruptedException e) {
        }
        client = new Client(HOST, PORT, this, handler);
        client.connect();
    }

    public void login(String username, String password) {
        persistLoginData(username, password);
        open();
        JSONObject request = requestGenerator.createLoginRequest(username, password);
        if (client.isConnected()) {
            client.send(request);
        } else {
            openingRequest = request;
        }
    }

    public void register(String username, String password, String password2, int passwordLength) {
        persistLoginData(username, password);
        open();
        JSONObject request = requestGenerator.createRegisterRequest(username, password, password2, passwordLength);
        if (client.isConnected()) {
            client.send(request);
        } else {
            openingRequest = request;
        }
    }

    private void persistLoginData(String username, String password) {
        storage.setUsername(username);
        storage.setPassword(password);
    }

    private void deleteLoginData() {
        storage.setUsername(null);
        storage.setPassword(null);
    }

    public void commitScore(int points, GameMode gameMode, GameLevel gameLevel, Date date) {
        JSONObject request = requestGenerator.createCommitScoreRequest(points, gameMode, gameLevel, date);
        if (client.isConnected()) {
            client.send(request);
        } else if (storage.isUserLoggedIn()) {
            cache.add(request);
        }
    }

    public void listScores(GameMode gameMode, GameLevel gameLevel, Date startDate, int startIndex, int pageSize) {
        JSONObject request = requestGenerator.createListScoresRequest(gameMode, gameLevel, startDate, startIndex, pageSize);
        if (client.isConnected()) {
            client.send(request);
        }
    }

    @Override
    public void onLoggedIn(String username) {
        if (listener != null) {
            listener.onLoggedIn(username);
        }
    }

    @Override
    public void onLogginError(String message) {
        deleteLoginData();
        if (listener != null) {
            listener.onLogginError(message);
        }
    }

    @Override
    public void onRegisterError(String message) {
        deleteLoginData();
        if (listener != null) {
            listener.onRegisterError(message);
        }
    }

    @Override
    public void onListScore(List<HighscoreListItem> highscores) {
        if (listener != null) {
            listener.onListScore(highscores);
        }
    }
}