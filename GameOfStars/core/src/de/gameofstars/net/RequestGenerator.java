package de.gameofstars.net;

import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by samuel on 01.03.15.
 */
public class RequestGenerator {
    private JSONObject createBasicRequest(String action) {
        JSONObject request = new JSONObject();
        request.put(RequestKeys.ACTION, action);
        return request;
    }

    public JSONObject createRegisterRequest(String username, String password, String password2, int passwordLength) {
        JSONObject request = createBasicRequest(RequestKeys.Action.REGISTER);
        request.put(RequestKeys.User.NAME, username);
        request.put(RequestKeys.User.PASSWORD, password);
        request.put(RequestKeys.User.PASSWORD2, password2);
        request.put(RequestKeys.User.PASSWORD_LENGTH, passwordLength);
        return request;
    }

    public JSONObject createLoginRequest(String username, String password) {
        JSONObject request = createBasicRequest(RequestKeys.Action.LOGIN);
        request.put(RequestKeys.User.NAME, username);
        request.put(RequestKeys.User.PASSWORD, password);
        return request;
    }

    public JSONObject createCommitScoreRequest(int points, GameMode gameMode, GameLevel gameLevel, Date date) {
        JSONObject request = createBasicRequest(RequestKeys.Action.COMMIT_SCORE);
        request.put(RequestKeys.Highscore.SCORE, points);
        request.put(RequestKeys.Highscore.GAME_MODE, gameMode.toString());
        request.put(RequestKeys.Highscore.GAME_LEVEL, gameLevel.toString());
        request.put(RequestKeys.Highscore.DATETIME, date.getTime());
        return request;
    }

    public JSONObject createListScoresRequest(GameMode gameMode, GameLevel gameLevel, Date startDate, int startIndex, int pageSize) {
        JSONObject request = createBasicRequest(RequestKeys.Action.LIST_SCORE);
        request.put(RequestKeys.Highscore.GAME_MODE, gameMode.toString());
        request.put(RequestKeys.Highscore.GAME_LEVEL, gameLevel.toString());
        request.put(RequestKeys.Highscore.LIST_START_DATE, startDate.getTime());
        request.put(RequestKeys.Highscore.LIST_START_INDEX, startIndex);
        request.put(RequestKeys.Highscore.LIST_SIZE, pageSize);
        return request;
    }
}