package de.gameofstars.net;

/**
 * Created by samuel on 01.03.15.
 */
public interface ClientListener {
    void onClientOpened();
    void onClientClosed();
}