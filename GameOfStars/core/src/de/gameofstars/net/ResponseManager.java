package de.gameofstars.net;

import de.gameofstars.gui.HighscoreListItem;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samuel on 28.02.15.
 */
public class ResponseManager implements ResponseHandler {
    private ResponseListener listener;

    public ResponseManager(ResponseListener listener) {
        this.listener = listener;
    }

    @Override
    public void handleResponse(JSONObject response) {
        if (response.has(RequestKeys.ACTION)) {
            switch (response.getString(RequestKeys.ACTION)) {
                case RequestKeys.Action.LOGIN:
                    handleLoginResponse(response);
                    break;
                case RequestKeys.Action.REGISTER:
                    handleRegisterResponse(response);
                    break;
                case RequestKeys.Action.LIST_SCORE:
                    handleListScoreResponse(response);
            }
        }
    }

    private void handleLoginResponse(JSONObject response) {
        if (response.has(RequestKeys.STATE)) {
            if (response.getBoolean(RequestKeys.STATE)) {
                listener.onLoggedIn(response.getString(RequestKeys.User.NAME));
            } else {
                listener.onLogginError(response.getString(RequestKeys.MESSAGE));
            }
        }
    }

    private void handleRegisterResponse(JSONObject response) {
        if (response.has(RequestKeys.STATE) && !response.getBoolean(RequestKeys.STATE)) {
            listener.onRegisterError(response.getString(RequestKeys.MESSAGE));
        }
    }

    private void handleListScoreResponse(JSONObject response) {
        if (response.has(RequestKeys.STATE) && response.getBoolean(RequestKeys.STATE) && response.has(RequestKeys.Highscore.LIST)) {
            List<HighscoreListItem> highscores = new ArrayList<HighscoreListItem>();
            JSONArray highscoreArray = response.getJSONArray(RequestKeys.Highscore.LIST);
            for (int i = 0; i < highscoreArray.length(); i++) {
                JSONObject highscoreElement = highscoreArray.getJSONObject(i);
                if (highscoreElement.has(RequestKeys.Highscore.POSITION) && highscoreElement.has(RequestKeys.User.NAME) && highscoreElement.has(RequestKeys.Highscore.SCORE)) {
                    int position = highscoreElement.getInt(RequestKeys.Highscore.POSITION);
                    String username = highscoreElement.getString(RequestKeys.User.NAME);
                    int points = highscoreElement.getInt(RequestKeys.Highscore.SCORE);
                    highscores.add(new HighscoreListItem(position, username, points));
                }
            }
            listener.onListScore(highscores);
        }
    }
}