package de.gameofstars.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import de.gameofstars.logic.Game;
import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import de.gameofstars.net.RequestKeys;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by samuel on 28.02.15.
 */
public class GuiManager implements StarterListener, BoardViewListener, HighscoreScreenListener {
    private static final float SCREEN_CHANGE_TIME = 0.3f;
    public static final float TOAST_ANIMATION_SECONDS = 0.5f;
    public static final float TOAST_SHOW_SECONDS = 5f;
    public static final String LOAD_HIGHSCORES = "Load Highscores...";

    private Game game;
    private Stage stage;
    private HashMap<ScreenIdentifier, ScreenView> screens;
    private ScreenIdentifier active;
    private LoadingDialog loadingDialog;
    private GameOfStars gameOfStars;
    private List<Label> toastLabels;
    private long toastShowTime;

    public GuiManager(GameOfStars gameOfStars) {
        this.gameOfStars = gameOfStars;
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setCatchMenuKey(true);
        screens = new HashMap<ScreenIdentifier, ScreenView>();
        game = new Game();
        stage = new Stage();
        stage.setViewport(new ScreenViewport());
        stage.addCaptureListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if(keycode == Input.Keys.BACK){
                    if (active == ScreenIdentifier.STARTER) {
                        Gdx.app.exit();
                    } else {
                        ScreenView activeScreenView = screens.get(active);
                        if (activeScreenView != null) {
                            if (activeScreenView.onBackPressed()) {
                                setActiveScreen(ScreenIdentifier.STARTER);
                            }
                        } else {
                            setActiveScreen(ScreenIdentifier.STARTER);
                        }
                    }
                    return false;
                }
                return super.keyDown(event, keycode);
            }
        });
        toastLabels = new ArrayList<Label>();
        StarterScreen starterScreen = new StarterScreen(this);
        screens.put(ScreenIdentifier.STARTER, starterScreen);
        GameScreen gameScreen = new GameScreen(game, this);
        screens.put(ScreenIdentifier.GAME, gameScreen);
        HighscoreScreen highscoreScreen = new HighscoreScreen(this);
        screens.put(ScreenIdentifier.HIGHSCORE, highscoreScreen);
        setActiveScreen(ScreenIdentifier.STARTER);
    }

    public void resize(int width, int height) {
        if (!toastLabels.isEmpty()) {
            Label message = toastLabels.get(0);
            message.setBounds((stage.getWidth() - message.getPrefWidth()) / 2, stage.getHeight() - message.getPrefHeight(), message.getPrefWidth(), message.getPrefHeight());
        }
        if (active != null) {
            screens.get(active).setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }
        if (stage != null) {
            stage.getViewport().update(width, height, true);
        }
    }

    public void render() {
        if (!toastLabels.isEmpty()) {
            final Label message = toastLabels.get(0);
            if (!stage.getActors().contains(message, true)) {
                toastShowTime = System.currentTimeMillis();
                stage.addActor(message);
                message.setPosition((stage.getWidth() - message.getPrefWidth()) / 2, stage.getHeight());
                message.addAction(Actions.moveBy(0, -message.getPrefHeight(), TOAST_ANIMATION_SECONDS, Interpolation.pow2));
            }
            if (message.getActions().size == 0 && toastShowTime != 0 && toastShowTime + (int) (TOAST_SHOW_SECONDS * 1000) <= System.currentTimeMillis()) {
                message.addAction(Actions.sequence(Actions.moveBy(0, message.getPrefHeight(), TOAST_ANIMATION_SECONDS, Interpolation.pow2), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        message.remove();
                        toastLabels.remove(message);
                    }
                })));
            }
        }
        stage.act();
        stage.draw();
    }

    @Override
    public void onGameStartedClicked(GameMode mode, GameLevel level) {
        game.start(mode, level);
        setActiveScreen(ScreenIdentifier.GAME);
    }

    @Override
    public void onLogin() {
        LoginDialog loginDialog = new LoginDialog(stage, LoginDialog.Type.LOGIN, gameOfStars);
        loginDialog.show();
    }

    @Override
    public void onRegister() {
        LoginDialog loginDialog = new LoginDialog(stage, LoginDialog.Type.REGISTER, gameOfStars);
        loginDialog.show();
    }

    @Override
    public void onShowHoghscore() {
        setActiveScreen(ScreenIdentifier.HIGHSCORE);
    }

    @Override
    public void onGameFinished() {
        int points = game.getCounter().getPoints();
        GameMode gameMode = game.getMode();
        GameLevel gameLevel = game.getLevel();
        gameOfStars.pushScore(points, gameMode, gameLevel);
        Gdx.input.setInputProcessor(stage);
        new GameFinishedDialog(this, points).show(stage);
    }

    public void setActiveScreen(ScreenIdentifier screen) {
        Gdx.input.setInputProcessor(null);
        if (active == screen) {
            return;
        }
        final ScreenView newScreen = screens.get(screen);
        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();
        newScreen.setSize(screenWidth, screenHeight);
        stage.addActor(newScreen);
        if (active != null) {
            float animationX = (screen == ScreenIdentifier.STARTER) ? screenWidth : -screenWidth;
            animateScreens(newScreen, animationX);
        }
        this.active = screen;
        Gdx.input.setInputProcessor(stage);
    }

    private void animateScreens(final ScreenView newScreen, float animationX) {
        newScreen.setPosition(-animationX, 0);
        newScreen.addAction(Actions.moveTo(0, 0, SCREEN_CHANGE_TIME, Interpolation.linear));
        final ScreenView activeScreen = screens.get(active);
        Action move = Actions.moveTo(animationX, 0, SCREEN_CHANGE_TIME, Interpolation.linear);
        activeScreen.addAction(Actions.sequence(move, Actions.run(new Runnable() {
            @Override
            public void run() {
                activeScreen.remove();
                newScreen.onShow();
            }
        })));
    }

    public void showToast(ToastType type, String message) {
        Label.LabelStyle skin = new Label.LabelStyle(GameOfStarsStyles.Fonts.MEDIUM, GameOfStarsStyles.Colors.INFO);
        skin.background = GameOfStarsStyles.Drawables.TOAST_INFO_BACKGROUND;
        if (type == ToastType.ERROR) {
            skin.fontColor = GameOfStarsStyles.Colors.ERROR;
            skin.background = GameOfStarsStyles.Drawables.TOAST_ERROR_BACKGROUND;
        }
        if (type == ToastType.SUCCESS) {
            skin.fontColor = GameOfStarsStyles.Colors.SUCCESS;
            skin.background = GameOfStarsStyles.Drawables.TOAST_SUCCESS_BACKGROUND;
        }
        Label messageLabel = new Label(message, skin);
        messageLabel.setZIndex(1000);
        toastLabels.add(messageLabel);
    }

    public void setLoggedIn(String username) {
        StarterScreen screen = (StarterScreen) screens.get(ScreenIdentifier.STARTER);
        screen.setLoggedIn(username);
    }

    public void setLoggedOut() {
        StarterScreen screen = (StarterScreen) screens.get(ScreenIdentifier.STARTER);
        screen.setLoggedOut();
    }

    public boolean isLoggedIn() {
        StarterScreen screen = (StarterScreen) screens.get(ScreenIdentifier.STARTER);
        return screen.isLoggedIn();
    }

    public void showLoginError(LoginDialog.Type type, String message) {
        setLoggedOut();
        LoginDialog loginDialog = new LoginDialog(stage, type, gameOfStars);
        loginDialog.setErrorMessage(message);
        loginDialog.show();
    }

    public void showLoadingDialog(String message) {
        if (!stage.getActors().contains(loadingDialog, true)) {
            loadingDialog = new LoadingDialog(message);
            loadingDialog.show(stage);
        }
    }

    public void disposeLoadingDialog() {
        if (loadingDialog != null && stage.getActors().contains(loadingDialog, true)) {
            loadingDialog.remove();
        }
    }

    public void showHighscoreList(final List<HighscoreListItem> highscores) {
        if (active == ScreenIdentifier.HIGHSCORE) {
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    HighscoreScreen screen = (HighscoreScreen) screens.get(active);
                    screen.setHighscoreList(highscores.toArray(new HighscoreListItem[highscores.size()]));
                    disposeLoadingDialog();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        setActiveScreen(ScreenIdentifier.STARTER);
    }

    @Override
    public void onLoadHighscoreList(GameMode gameMode, GameLevel gameLevel, Date startDate, int startIndex, int pageSize) {
        showLoadingDialog(LOAD_HIGHSCORES);
        gameOfStars.loadHighscoreList(gameMode, gameLevel, startDate, startIndex, pageSize);
    }
}