package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import de.gameofstars.logic.Counter;
import de.gameofstars.logic.CounterListener;

/**
 * Created by samuel on 24.02.15.
 */
public class HeaderView extends WidgetGroup implements CounterListener {
    private Shape background;
    private Label points;
    private Label movesLeft;

    public HeaderView(Counter counter) {
        counter.addCounterListener(this);
        background = new Shape();
        background.setColor(Color.BLACK);
        addActor(background);
        Label.LabelStyle labelStyle = new Label.LabelStyle(GameOfStarsStyles.Fonts.MEDIUM, Color.BLACK);
        labelStyle.background = GameOfStarsStyles.Drawables.LABEL_BACKGROUND;
        points = new Label("Score: 0", labelStyle);
        points.setPosition(0, 0);
        addActor(points);
        movesLeft = new Label("Moves: 20", labelStyle);
        movesLeft.setPosition(100, 0);
        addActor(movesLeft);
    }

    public void resize() {
        float borderPadding = 10f;
        background.setPosition(getX(), getY());
        background.setSize(getWidth(), getHeight());
        movesLeft.setWidth(movesLeft.getPrefWidth());
        movesLeft.setPosition(getWidth() - movesLeft.getWidth() - borderPadding, getHeight() - movesLeft.getHeight() - borderPadding);
        points.setWidth(points.getPrefWidth());
        points.setPosition(getWidth() - points.getWidth() - borderPadding, movesLeft.getY() - points.getHeight() - borderPadding);
    }

    @Override
    public void onMovesChanged(int movesLeft) {
        this.movesLeft.setText("Moves: " + movesLeft);
        resize();
    }

    @Override
    public void onPointsChanged(int points) {
        this.points.setText("Score: " + points);
        resize();
    }

    @Override
    public void onTimeChanged(int secondsLeft) {
        this.movesLeft.setText("Time: " + secondsLeft);
    }
}
