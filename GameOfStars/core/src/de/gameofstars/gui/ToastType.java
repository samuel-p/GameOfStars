package de.gameofstars.gui;

public enum ToastType {
        INFO, SUCCESS, ERROR
    }