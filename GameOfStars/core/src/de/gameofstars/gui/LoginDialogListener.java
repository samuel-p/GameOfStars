package de.gameofstars.gui;

/**
 * Created by samuel on 28.02.15.
 */
public interface LoginDialogListener {
    void onLogin(String username, String password);

    void onRegister(String username, String password, String password2);
}