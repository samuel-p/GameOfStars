package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Created by samuel on 04.03.15.
 */
public class HighscoreList extends Table {
    private final Label.LabelStyle labelStyle;
    private final Label.LabelStyle labelStyle2;

    public HighscoreList() {
        labelStyle = new Label.LabelStyle(GameOfStarsStyles.Fonts.MEDIUM, Color.BLACK);
        labelStyle2 = new Label.LabelStyle(labelStyle);
        labelStyle2.background = GameOfStarsStyles.Drawables.LIST_BACKGROUND_2;
    }

    public void setItems(HighscoreListItem[] items) {
        reset();
        align(Align.top);
        for (int i = 0; i < items.length; i++) {
            HighscoreListItem item = items[i];
            Label.LabelStyle style = (i % 2 == 0) ? labelStyle2 : labelStyle;
            Actor positionLabel = getPositionLabel(item, style);
            Label usernameLabel = new Label(item.getUsername(), style);
            usernameLabel.setAlignment(Align.center);
            Label pointsLabel = new Label(String.valueOf(item.getPoints()), style);
            pointsLabel.setAlignment(Align.center);
            float height = calcHeight(15, usernameLabel.getPrefHeight(), pointsLabel.getPrefHeight());
            add(positionLabel).height(height).width(height);
            add(usernameLabel).expandX().fill();
            add(pointsLabel).expandX().fill();
            row();
        }
    }

    private float calcHeight(int padding, float ... heights) {
        float max = 0;
        for (float height : heights) {
            if (height > max) {
                max = height;
            }
        }
        return max + padding;
    }

    private Actor getPositionLabel(HighscoreListItem item, Label.LabelStyle style) {
        if (item.getPosition() > 3) {
            Label positionLabel = new Label(String.valueOf(item.getPosition()), style);
            positionLabel.setAlignment(Align.center);
            return positionLabel;
        } else {
            Image image = new PositionImage(GameOfStarsStyles.Drawables.CUP_GOLD, style);
            if (item.getPosition() == 2) {
                image.setDrawable(GameOfStarsStyles.Drawables.CUP_SILVER);
            }
            if (item.getPosition() == 3) {
                image.setDrawable(GameOfStarsStyles.Drawables.CUP_BRONZE);
            }
            return image;
        }
    }

    private class PositionImage extends Image {
        private Label.LabelStyle labelStyle;

        public PositionImage(Drawable drawable, Label.LabelStyle labelStyle) {
            super(drawable);
            this.labelStyle = labelStyle;
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            if (labelStyle.background != null) {
                labelStyle.background.draw(batch, getX(), getY(), getWidth(), getHeight());
            }
            super.draw(batch, parentAlpha);
        }
    }
}