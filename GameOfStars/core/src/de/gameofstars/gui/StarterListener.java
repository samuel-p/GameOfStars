package de.gameofstars.gui;

import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;

/**
 * Created by samuel on 25.02.15.
 */
public interface StarterListener {
    void onGameStartedClicked(GameMode mode, GameLevel level);

    void onLogin();

    void onRegister();

    void onShowHoghscore();
}