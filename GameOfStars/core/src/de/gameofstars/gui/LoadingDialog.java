package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.*;

/**
 * Created by samuel on 01.03.15.
 */
public class LoadingDialog extends Dialog {
    private static final int stepMillis = 100;

    private Image load;
    private int lastIndex;
    private long lastChange;

    public LoadingDialog(String loadingMessage) {
        this(loadingMessage, null);
    }

    public LoadingDialog(String dialogTitle, String loadingMessage) {
        super(dialogTitle, createSkin());
        if (loadingMessage != null) {
            Label.LabelStyle messageSkin = new Label.LabelStyle(GameOfStarsStyles.Fonts.MEDIUM, Color.BLACK);
            Label message = new Label(loadingMessage, messageSkin);
            message.setWrap(true);
            getContentTable().add(message).fill();
            getContentTable().row();
        }
        load  = new Image(GameOfStarsStyles.Loading.DRAWABLES[0]);
        float radius = getStyle().titleFont.getLineHeight() * 2;
        getContentTable().add(load).width(radius).height(radius);
        lastChange = System.currentTimeMillis();
        pad(getStyle().titleFont.getLineHeight() + 20, 60, 15, 60);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        long now = System.currentTimeMillis();
        if (lastChange + stepMillis < now) {
            lastIndex = (lastIndex + 1) % GameOfStarsStyles.Loading.DRAWABLES.length;
            load.setDrawable(GameOfStarsStyles.Loading.DRAWABLES[lastIndex]);
            lastChange = now;
        }
    }

    private static WindowStyle createSkin() {
        Window.WindowStyle skin = new Window.WindowStyle(GameOfStarsStyles.Fonts.BIG, Color.BLACK, GameOfStarsStyles.Drawables.DIALOG_BACKGROUND);
        return skin;
    }
}