package de.gameofstars.gui;

public enum ScreenIdentifier {
    STARTER, HIGHSCORE, GAME
}