package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by samuel on 01.03.15.
 */
public class HighscoreScreen extends ScreenView {
    private static final String[] DATE_SELECTION_ITEMS = new String[]{"Today", "This Week", "This Month", "This Year"};
    private static final String[] MODE_SELECTION_ITEMS = new String[]{"Normal Mode", "Time Mode"};

    private final ScrollPane scrollPane;
    private final HighscoreList highscoreList;
    private final SelectBox<String> dateSelection;
    private final SelectBox<String> modeSelection;
    private final ImageButton backButton;
    private HighscoreScreenListener listener;

    public HighscoreScreen(final HighscoreScreenListener listener) {
        this.listener = listener;
        highscoreList = new HighscoreList();
        scrollPane = new ScrollPane(highscoreList);
        addActor(scrollPane);
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle(GameOfStarsStyles.Drawables.BUTTON_NOTROUNDED_LEFT, GameOfStarsStyles.Drawables.BUTTON_NOTROUNDED_LEFT_PRESSED, null, GameOfStarsStyles.Drawables.ARROW_LEFT, GameOfStarsStyles.Drawables.ARROW_LEFT, null);
        backButton = new ImageButton(style);
        backButton.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (listener != null) {
                    listener.onBackPressed();
                }
            }
        });
        addActor(backButton);
        List.ListStyle selectBoxListSkin = new List.ListStyle(GameOfStarsStyles.Fonts.MEDIUM, Color.BLACK, Color.BLACK, GameOfStarsStyles.Drawables.TEXT_SELECTION);
        ScrollPane.ScrollPaneStyle scrollPaneStyle = new ScrollPane.ScrollPaneStyle(new ScrollPane.ScrollPaneStyle(scrollPane.getStyle()));
        scrollPaneStyle.background = GameOfStarsStyles.Drawables.SELECTBOX_DROPDOWN_BACKGROUND;
        SelectBox.SelectBoxStyle selectBoxSkin = new SelectBox.SelectBoxStyle(GameOfStarsStyles.Fonts.BIG, Color.BLACK, GameOfStarsStyles.Drawables.BUTTON_CENTER_H, scrollPaneStyle, selectBoxListSkin);
        selectBoxSkin.backgroundOpen = GameOfStarsStyles.Drawables.BUTTON_CENTER_H_PRESSED;
        dateSelection = new SelectBox<String>(selectBoxSkin);
        dateSelection.setItems(DATE_SELECTION_ITEMS);
        dateSelection.addCaptureListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                reload();
            }
        });
        addActor(dateSelection);
        SelectBox.SelectBoxStyle selectBoxSkin2 = new SelectBox.SelectBoxStyle(selectBoxSkin);
        selectBoxSkin2.background = GameOfStarsStyles.Drawables.BUTTON_NOTROUNDED_RIGHT;
        selectBoxSkin2.backgroundOpen = GameOfStarsStyles.Drawables.BUTTON_NOTROUNDED_RIGHT_PRESSED;
        modeSelection = new SelectBox<String>(selectBoxSkin2);
        modeSelection.setItems(MODE_SELECTION_ITEMS);
        modeSelection.addCaptureListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                reload();
            }
        });
        addActor(modeSelection);
    }

    @Override
    public void resize() {
        float height = dateSelection.getPrefHeight();
        float width = (getWidth() - height) / 2;
        backButton.setBounds(0, getHeight() - height, height, height);
        dateSelection.setBounds(backButton.getX() + backButton.getWidth(), getHeight() - height, width, height);
        modeSelection.setBounds(dateSelection.getX() + dateSelection.getWidth(), dateSelection.getY(), width, height);
        scrollPane.setSize(getWidth(), getHeight() - dateSelection.getHeight());
        if (scrollPane.getHeight() > highscoreList.getHeight()) {
            highscoreList.setHeight(scrollPane.getHeight());
        }
    }

    @Override
    public void onShow() {
        reload();
    }

    private void reload() {
        GameLevel gameLevel = GameLevel.NORMAL;
        GameMode gameMode = GameMode.values()[modeSelection.getSelectedIndex()];
        Date startDate = getSelectedStartDate().getTime();
        if (listener != null) {
            listener.onLoadHighscoreList(gameMode, gameLevel, startDate, 0, 100);
        }
    }

    private Calendar getSelectedStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        if (dateSelection.getSelectedIndex() == 1) {
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        }
        if (dateSelection.getSelectedIndex() == 2) {
            calendar.set(Calendar.DAY_OF_MONTH, 1);
        }
        if (dateSelection.getSelectedIndex() == 3) {
            calendar.set(Calendar.DAY_OF_YEAR, 1);
        }
        return calendar;
    }

    public void setHighscoreList(HighscoreListItem[] highscores) {
        highscoreList.setItems(highscores);
        resize();
    }
}