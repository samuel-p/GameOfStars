package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;

/**
 * Created by samuel on 25.02.15.
 */
public class StarterScreen extends ScreenView {
    public static final String START_NORMAL_MODE = "Start Normal Mode";
    public static final String START_TIME_MODE = "Start Time Mode";
    public static final String LOGIN = "Login";
    public static final String REGISTER = "Register";
    public static final String WELCOME_BACK = "Welcome back ";
    public static final String EXCLAMATION_MARK = "!";
    public static final String HIGHSCORE = "Highscore";

    private final TextButton.TextButtonStyle topBtn;
    private final TextButton.TextButtonStyle centerBtn;
    private final TextButton.TextButtonStyle bottomBtn;

    private Image logo;
    private TextButton startSingleNormal;
    private TextButton startSingleTime;
    private TextButton showHighscore;

    private TextButton login;
    private TextButton register;
    private Label welcomeBack;
    private StarterListener listener;

    public StarterScreen(final StarterListener listener) {
        this.listener = listener;
        setColor(Color.BLACK);
        logo = new Image(GameOfStarsStyles.Textures.LOGO);
        Action zoom = Actions.sequence(Actions.scaleTo(0.9f, 0.9f, 1f), Actions.scaleTo(1f, 1f, 1f));
        Action fade = Actions.sequence(Actions.alpha(0.6f, 1f), Actions.alpha(1f, 1f));
        logo.addAction(Actions.forever(Actions.parallel(zoom, fade)));
        addActor(logo);
        topBtn = new TextButton.TextButtonStyle(GameOfStarsStyles.Drawables.BUTTON_TOP, GameOfStarsStyles.Drawables.BUTTON_TOP_PRESSED, null, GameOfStarsStyles.Fonts.BIG);
        topBtn.fontColor = Color.BLACK;
        centerBtn = new TextButton.TextButtonStyle(topBtn);
        centerBtn.up = GameOfStarsStyles.Drawables.BUTTON_CENTER_V;
        centerBtn.down = GameOfStarsStyles.Drawables.BUTTON_CENTER_V_PRESSED;
        bottomBtn = new TextButton.TextButtonStyle(topBtn);
        bottomBtn.up = GameOfStarsStyles.Drawables.BUTTON_BOTTOM;
        bottomBtn.down = GameOfStarsStyles.Drawables.BUTTON_BOTTOM_PRESSED;
        startSingleNormal = new TextButton(START_NORMAL_MODE, topBtn);
        startSingleNormal.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                invokeGameStart(GameMode.SINGLE_PLAYER_NORMAL);
            }
        });
        addActor(startSingleNormal);
        startSingleTime = new TextButton(START_TIME_MODE, bottomBtn);
        startSingleTime.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                invokeGameStart(GameMode.SINGLE_PLAYER_TIME);
            }
        });
        addActor(startSingleTime);
        showHighscore = new TextButton(HIGHSCORE, bottomBtn);
        showHighscore.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (listener != null) {
                    listener.onShowHoghscore();
                }
            }
        });
        TextButton.TextButtonStyle leftBtn = new TextButton.TextButtonStyle(GameOfStarsStyles.Drawables.BUTTON_LEFT, GameOfStarsStyles.Drawables.BUTTON_LEFT_PRESSED, null, GameOfStarsStyles.Fonts.MEDIUM);
        leftBtn.fontColor = Color.BLACK;
        login = new TextButton(LOGIN, leftBtn);
        login.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (StarterScreen.this.listener != null) {
                    StarterScreen.this.listener.onLogin();
                }
            }
        });
        addActor(login);
        TextButton.TextButtonStyle rightBtn = new TextButton.TextButtonStyle(GameOfStarsStyles.Drawables.BUTTON_RIGHT, GameOfStarsStyles.Drawables.BUTTON_RIGHT_PRESSED, null, GameOfStarsStyles.Fonts.MEDIUM);
        rightBtn.fontColor = Color.BLACK;
        register = new TextButton(REGISTER, rightBtn);
        register.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (StarterScreen.this.listener != null) {
                    StarterScreen.this.listener.onRegister();
                }
            }
        });
        addActor(register);
        Label.LabelStyle wbSkin = new Label.LabelStyle(GameOfStarsStyles.Fonts.BIG, Color.WHITE);
        welcomeBack = new Label("", wbSkin);
    }

    private void invokeGameStart(GameMode mode) {
        if (listener != null) {
            listener.onGameStartedClicked(mode, GameLevel.NORMAL);
        }
    }

    @Override
    public void resize() {
        float paddingTop = getHeight() * 0.05f;
        float width = calcWidth();
        float height = calcHeight();
        login.setSize(width / 2, login.getPrefHeight());
        register.setSize(width / 2, register.getPrefHeight());
        welcomeBack.setBounds((getWidth() - width) / 2, getHeight() - paddingTop - login.getHeight(), width, height);
        login.setPosition(welcomeBack.getX(), welcomeBack.getY());
        register.setPosition(login.getX() + login.getWidth(), login.getY());
        startSingleNormal.setSize(width, height);
        startSingleTime.setSize(width, height);
        showHighscore.setSize(width, height);
        startSingleNormal.setPosition((getWidth() - width) / 2, getHeight() / 4);
        startSingleTime.setPosition((getWidth() - width) / 2, startSingleNormal.getY() - height);
        showHighscore.setPosition((getWidth() - width) / 2, startSingleTime.getY() - height);
        float logoRadius = login.getY() - startSingleNormal.getY() - startSingleNormal.getHeight();
        if (logoRadius > width) {
            logoRadius = width;
        }
        float padTop = login.getY() - startSingleNormal.getY() - startSingleNormal.getHeight();
        logo.setSize(logoRadius, logoRadius);
        logo.setPosition((getWidth() - logoRadius) / 2, login.getY() - padTop + (padTop - logoRadius) / 2);
        logo.setOrigin(logo.getWidth() / 2, logo.getHeight() / 2);
    }

    private float calcWidth() {
        float width = startSingleNormal.getPrefWidth();
        if (width < startSingleTime.getPrefWidth()) {
            width = startSingleTime.getPrefWidth();
        }
        if (width < welcomeBack.getPrefWidth()) {
            width = welcomeBack.getPrefWidth();
        }
        return width;
    }

    private float calcHeight() {
        float height = startSingleNormal.getPrefHeight();
        if (height < startSingleTime.getPrefHeight()) {
            height = startSingleTime.getPrefHeight();
        }
        if (height < welcomeBack.getPrefHeight()) {
            height = welcomeBack.getPrefHeight();
        }
        return height;
    }

    public void setLoggedIn(String username) {
        if ((username == null || username.length() == 0) && !isLoggedIn()) {
            // TODO log out
        } else {
            login.remove();
            register.remove();
            welcomeBack.setText(WELCOME_BACK + username + EXCLAMATION_MARK);
            addActor(welcomeBack);
            addActor(showHighscore);
            startSingleTime.setStyle(centerBtn);
            resize();
        }
    }

    public void setLoggedOut() {
        setLoggedIn(null);
    }

    public boolean isLoggedIn() {
        return getChildren().contains(welcomeBack, true);
    }
}