package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Arrays;

public class Shape extends Actor {
    private ShapeRenderer shapeRenderer;

    private boolean filled;
    private Color borderColor;
    private float borderWidth;
    private Color[] gradient;

    public Shape() {
        this(0, 0, 0, 0);
    }

    public Shape(float x, float y, float width, float height) {
        this.shapeRenderer = new ShapeRenderer();
        setPosition(x, y);
        setSize(width, height);
        borderColor = Color.WHITE;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.setAutoShapeType(true);
        shapeRenderer.begin();
        if (filled) {
            shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
            if (gradient != null && gradient.length == 4) {
                shapeRenderer.rect(getX(), getY(), getWidth(), getHeight(), gradient[0], gradient[1], gradient[2], gradient[3]);
            } else {
                shapeRenderer.setColor(getColor());
                shapeRenderer.rect(getX(), getY(), getWidth(), getHeight());
            }
        }
        shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(borderColor);
        float padOut = borderWidth / 2;
        shapeRenderer.rect(getX() - padOut, getY() - padOut, borderWidth, getHeight() + borderWidth);
        shapeRenderer.rect(getX() - padOut, getY() - padOut, getWidth() + borderWidth, borderWidth);
        shapeRenderer.rect(getX() - padOut, getY() + getHeight() -padOut, getWidth() + borderWidth, borderWidth);
        shapeRenderer.rect(getX() + getWidth() - padOut, getY() - padOut, borderWidth, getHeight() + borderWidth);
        shapeRenderer.end();
        batch.begin();
    }

    @Override
    public void setColor(Color color) {
        super.setColor(color);
        setFilled(color != null);
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }

    public float getBorderWidth() {
        return borderWidth;
    }

    public void setGradient(Color bottomLeftColor, Color bottomRightColor, Color topRightColor, Color topLeftColor) {
        if (bottomLeftColor == null || bottomRightColor == null || topRightColor == null || topLeftColor == null) {
            return;
        }
        this.gradient = new Color[] {bottomLeftColor, bottomRightColor, topRightColor, topLeftColor};
        setFilled(true);
    }

    public Color[] getGradient() {
        return gradient;
    }
}