package de.gameofstars.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import de.gameofstars.logic.*;

import java.util.Map;
import java.util.Random;

/**
 * Created by samuel on 22.02.15.
 */
public class BoardView extends WidgetGroup implements FieldActionListener.FieldSwitchedListener, BoardListener, GameListener {
    private static final boolean HINTS_ENABLED = true;

    private static final float SWITCH_TIME_SECONDS = 0.3f;
    private static final float IMAGE_DISPOSE_SECONDS = 0.4f;
    private static final float MOVE_SINGLE_IMAGE_SECONDS = 1f;
    private static final float SHOW_FIELD_SECONDS = 0.7f;
    private static final float ROTATE_SECONDS = 5f;

    private static final float PAD_PERCENTAGE = 0.03f;

    private Shape background;
    private Shape topBlock;
    private Shape gameArea;
    private Image[][] fields;
    private Board board;
    private float fieldWidth;
    private float fieldHeight;
    private Thread hintDelay;
    private boolean gameRunning;
    private BoardViewListener listener;

    public BoardView(Game game, BoardViewListener listener) {
        this.listener = listener;
        game.addGameListener(this);
        this.board = game.getBoard();
        this.board.addBoardListener(this);
        background = new Shape();
        background.setColor(Color.BLACK);
        background.setBorderWidth(0);
        addActor(background);
        gameArea = new Shape();
        gameArea.setGradient(Color.LIGHT_GRAY, Color.GRAY, Color.LIGHT_GRAY, new Color(0xeeeeeeff));
        gameArea.setBorderColor(Color.BLACK);
        gameArea.setBorderWidth(1);
        addActor(gameArea);
        fields = new Image[Board.HEIGHT][Board.WIDTH];
        for (Field field : board.getFields()) {
            Image image = createImage(field);
            fields[field.getY()][field.getX()] = image;
            addActor(image);
        }
        topBlock = new Shape();
        topBlock.setColor(Color.BLACK);
        topBlock.setBorderWidth(0);
        addActor(topBlock);
    }

    private Image createImage(Field field) {
        Image image = new Image(GameOfStarsStyles.Textures.STAR);
        image.addListener(new FieldActionListener(image, field, this));
        return image;
    }

    public void resize() {
        float boardWidth = getWidth() - 2 * getWidth() * PAD_PERCENTAGE;
        float boardHeight = getHeight() - 2 * getHeight() * PAD_PERCENTAGE;
        if (boardWidth > boardHeight) {
            boardWidth = boardHeight;
        } else {
            boardHeight = boardWidth;
        }
        fieldWidth = boardWidth / Board.WIDTH;
        fieldHeight = boardHeight / Board.HEIGHT;
        float padBoardLeft =
                (getWidth() - boardWidth) / 2;
        float padBoardBottom = (getHeight() - boardHeight) / 2;
        background.setPosition(getX(), getY());
        background.setSize(getWidth(), getHeight());
        topBlock.setPosition(getX(), getY() + getHeight() - padBoardBottom);
        topBlock.setSize(getWidth(), padBoardBottom);
        gameArea.setPosition(getX() + padBoardLeft, getY() + padBoardBottom);
        gameArea.setSize(boardWidth, boardHeight);
        for (int y = 0; y < fields.length; y++) {
            for (int x = 0; x < fields[y].length; x++) {
                fields[y][x].setWidth(fieldWidth);
                fields[y][x].setHeight(fieldHeight);
                fields[y][x].setOrigin(fieldWidth / 2, fieldHeight / 2);
                fields[y][x].setPosition(padBoardLeft + x * fieldWidth, padBoardBottom + (fields.length - y - 1) * fieldHeight);
            }
        }
    }

    private void repaint() {
        for (Field field : board.getFields()) {
            Image image = fields[field.getY()][field.getX()];
            if (image != null) {
                image.clearActions();
                image.setScale(1, 1);
                image.setRotation(0);
                image.setColor(field.getColor().getColor());
            }
        }
    }

    private void addAllColorsAnimation(Image image) {
        SequenceAction randomizeColor = Actions.sequence();
        for (FieldColor color : FieldColor.values()) {
            if (color.getColor() != null) {
                randomizeColor.addAction(Actions.color(color.getColor(), 0.3f));
            }
        }
        image.addAction(Actions.forever(randomizeColor));
    }

    @Override
    public void onFieldsSwitched(final Field field1, final Field field2) {
        Gdx.input.setInputProcessor(null);
        AnimationGroup animation = switchFieldAnimation(field1, field2);
        animation.setListener(new AnimationGroup.AnimationListener() {
            @Override
            public void onAnimationStarted() {

            }

            @Override
            public void onAnimationEnded() {
                board.swapFields(field1, field2);
            }
        });
        animation.animate();
    }

    private AnimationGroup switchFieldAnimation(final Field field1, final Field field2) {
        float animationWidthX = (field1.getX() - field2.getX()) * fieldWidth;
        float animationWidthY = (field1.getY() - field2.getY()) * fieldHeight;
        AnimationGroup animation = new AnimationGroup();
        Image image1 = fields[field1.getY()][field1.getX()];
        animation.putAnimation(image1, Actions.moveBy(-animationWidthX, animationWidthY, SWITCH_TIME_SECONDS, Interpolation.linear));
        Image image2 = fields[field2.getY()][field2.getX()];
        animation.putAnimation(image2, Actions.moveBy(animationWidthX, -animationWidthY, SWITCH_TIME_SECONDS, Interpolation.linear));
        return animation;

    }

    @Override
    public void onFieldsCleared(final Field[] clearedFields, int[] clearedGroupSizes) {
        cancelHintDelay();
        AnimationGroup animation = new AnimationGroup();
        for (Field field : clearedFields) {
            Image image = this.fields[field.getY()][field.getX()];
            Action scale = Actions.scaleTo(2, 2, IMAGE_DISPOSE_SECONDS, Interpolation.pow2);
            Action fade = Actions.fadeOut(IMAGE_DISPOSE_SECONDS, Interpolation.pow2);
            animation.putAnimation(image, Actions.parallel(scale, fade));
        }
        animation.setListener(new AnimationGroup.AnimationListener() {
            @Override
            public void onAnimationStarted() {

            }

            @Override
            public void onAnimationEnded() {
                board.moveFields();
            }
        });
        animation.animate();
    }

    @Override
    public void onFieldsSwapped(Field field1, Field field2) {
        repaint();
        resize();
    }

    @Override
    public void onFieldsNotSwapped(Field field1, Field field2) {
        AnimationGroup animation = switchFieldAnimation(field1, field2);
        animation.setListener(new AnimationGroup.AnimationListener() {
            @Override
            public void onAnimationStarted() {

            }

            @Override
            public void onAnimationEnded() {
                repaint();
                resize();
                Gdx.input.setInputProcessor(getStage());
                startHintDelay();
            }
        });
        animation.animate();
    }

    @Override
    public void onShuffled() {
        repaint();
        resize();
        AnimationGroup animation = new AnimationGroup();
        for (Field field : board.getFields()) {
            Image image = fields[field.getY()][field.getX()];
            image.setScale(0, 0);
            animation.putAnimation(image, Actions.scaleTo(1, 1, SHOW_FIELD_SECONDS, Interpolation.pow2));
        }
        animation.setListener(new AnimationGroup.AnimationListener() {
            @Override
            public void onAnimationStarted() {

            }

            @Override
            public void onAnimationEnded() {
                startHintDelay();
            }
        });
        animation.animate();
    }

    @Override
    public void onFieldsMovedDown(Map<Field, Integer> fields) {
        repaint();
        resize();
        AnimationGroup animation = new AnimationGroup();
        Random r = new Random();
        for (Field field : fields.keySet()) {
            Image image = this.fields[field.getY()][field.getX()];
            float animationHeight = fields.get(field) * fieldHeight;
            image.setPosition(image.getX(), image.getY() + animationHeight);
            Action down = Actions.moveBy(0, -animationHeight, MOVE_SINGLE_IMAGE_SECONDS, Interpolation.bounceOut);
            animation.putAnimation(image, Actions.sequence(Actions.delay(r.nextFloat() / 10), down));
        }
        animation.setListener(new AnimationGroup.AnimationListener() {
            @Override
            public void onAnimationStarted() {
            }

            @Override
            public void onAnimationEnded() {
                if (!board.clearFields()) {
                    if (gameRunning) {
                        Gdx.input.setInputProcessor(getStage());
                        startHintDelay();
                    } else if (listener != null) {
                        listener.onGameFinished();
                    }
                }
            }
        });
        animation.animate();
    }

    private void startHintDelay() {
        hintDelay = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            showPossibleSwitches();
                        }
                    });
                } catch (InterruptedException e) {
                }
            }
        });
        if (HINTS_ENABLED) {
            hintDelay.start();
        }
    }

    private void cancelHintDelay() {
        if (hintDelay != null) {
            hintDelay.interrupt();
            hintDelay = null;
        }
    }

    private void showPossibleSwitches() {
        Field[] fields = board.searchPossibleSwitches();
        for (Field field : fields) {
            this.fields[field.getY()][field.getX()].addAction(Actions.forever(Actions.rotateBy(360, ROTATE_SECONDS)));
        }
    }

    @Override
    public void onGameStarted(GameMode mode, GameLevel level) {
        gameRunning = true;
    }

    @Override
    public void onGameEnd(GameMode mode, GameLevel level) {
        gameRunning = false;
        if (Gdx.input.getInputProcessor() != null && listener != null) {
            listener.onGameFinished();
        }
    }
}