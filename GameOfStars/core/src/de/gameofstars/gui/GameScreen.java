package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import de.gameofstars.logic.Game;

import java.io.ByteArrayInputStream;

/**
 * Created by samuel on 25.02.15.
 */
public class GameScreen extends ScreenView {

    private BoardView boardView;
    private HeaderView headerView;

    public GameScreen( Game game, BoardViewListener listener) {
        setColor(Color.BLUE);
        boardView = new BoardView(game, listener);
        addActor(boardView);
        headerView = new HeaderView(game.getCounter());
        addActor(headerView);
    }

    @Override
    public void resize() {
        headerView.setPosition(getX(), getHeight() * 0.8f);
        headerView.setSize(getWidth(), getHeight() * 0.2f);
        headerView.resize();
        boardView.setPosition(getX(), getY());
        boardView.setSize(getWidth(), getHeight() * 0.8f);
        boardView.resize();
    }

    @Override
    public void onShow() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}