package de.gameofstars.gui;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import de.gameofstars.data.Storage;
import de.gameofstars.encryption.Hash;
import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;
import de.gameofstars.net.ClientManager;
import de.gameofstars.net.ResponseListener;
import de.gameofstars.net.ResponseManager;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

public class GameOfStars extends ApplicationAdapter implements ResponseListener, LoginDialogListener {
    public static final String LOGIN = "Login...";
    public static final String REGISTER = "Register...";
    public static final String LOGGED_IN = " logged in!";
    private GuiManager guiManager;
    private Storage storage;
    private ClientManager client;

    @Override
    public void create() {
        guiManager = new GuiManager(this);
        storage = new Storage();
        client = new ClientManager(storage, this);
        if (storage.isUserLoggedIn()) {
             guiManager.setLoggedIn(storage.getUsername());
             guiManager.showToast(ToastType.SUCCESS, storage.getUsername() + LOGGED_IN);
        }
    }

    @Override
    public void resize(int width, int height) {
        guiManager.resize(width, height);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        guiManager.render();
    }

    @Override
    public void onLoggedIn(final String username) {
        disposeLoadingDialog();
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (!guiManager.isLoggedIn()) {
                    guiManager.setLoggedIn(username);
                    guiManager.showToast(ToastType.SUCCESS, username + LOGGED_IN);
                }
            }
        });
    }

    @Override
    public void onLogginError(String message) {
        disposeLoadingDialog();
        guiManager.showLoginError(LoginDialog.Type.LOGIN, message);
    }

    @Override
    public void onRegisterError(String message) {
        disposeLoadingDialog();
        guiManager.showLoginError(LoginDialog.Type.REGISTER, message);
    }

    @Override
    public void onListScore(List<HighscoreListItem> highscores) {
        guiManager.showHighscoreList(highscores);
    }

    @Override
    public void onLogin(String username, String password) {
        try {
            guiManager.showLoadingDialog(LOGIN);
            client.login(username, hashPassword(password));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            // TODO
            e.printStackTrace();
        }
    }

    @Override
    public void onRegister(String username, String password, String password2) {
        try {
            guiManager.showLoadingDialog(REGISTER);
            client.register(username, hashPassword(password), hashPassword(password2), password.length());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            // TODO
            e.printStackTrace();
        }
    }

    private String hashPassword(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return Hash.generateMD5(Hash.generateSHA512(password));
    }

    private void disposeLoadingDialog() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                guiManager.disposeLoadingDialog();
            }
        });
    }

    public void pushScore(int points, GameMode gameMode, GameLevel gameLevel) {
        Date date = new Date(System.currentTimeMillis());
        client.commitScore(points, gameMode, gameLevel, date);
    }

    public void loadHighscoreList(GameMode gameMode, GameLevel gameLevel, Date startDate, int startIndex, int pageSize) {
        client.listScores(gameMode, gameLevel, startDate, startIndex, pageSize);
    }
}