package de.gameofstars.gui;

public class HighscoreListItem {

    private int position;
    private String username;
    private int points;

    public HighscoreListItem(int position, String username, int points) {
        this.position = position;
        this.username = username;
        this.points = points;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}