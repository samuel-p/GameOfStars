package de.gameofstars.gui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

/**
 * Created by samuel on 25.02.15.
 */
public abstract class ScreenView extends WidgetGroup {
    private ShapeRenderer shapeRenderer;

    public ScreenView() {
        this.shapeRenderer = new ShapeRenderer();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(getColor());
        shapeRenderer.rect(getX(), getY(), getWidth(), getHeight());
        shapeRenderer.end();
        batch.begin();
        super.draw(batch, parentAlpha);
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        resize();
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);
        resize();
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        resize();
    }

    @Override
    public void setPosition(float x, float y, int alignment) {
        super.setPosition(x, y, alignment);
        resize();
    }


    @Override
    public void setX(float x) {
        super.setX(x);
        resize();
    }

    @Override
    public void setY(float y) {
        super.setY(y);
        resize();
    }

    @Override
    public void setWidth(float width) {
        super.setWidth(width);
        resize();
    }

    @Override
    public void setHeight(float height) {
        super.setHeight(height);
        resize();
    }

    public abstract void resize();

    public void onShow(){
    }

    public boolean onBackPressed() {
        return true;
    }
}