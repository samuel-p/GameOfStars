package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by samuel on 28.02.15.
 */
public class LoginDialog extends Dialog {
    private static final String TITLE_LOGIN = "Login:";
    private static final String TITLE_REGISTER = "Register:";
    private static final String ENTER_USERNAME = "Enter username:";
    private static final String ENTER_PASSWORD = "Enter password:";
    private static final String REPEAT_PASSWORD = "Repeat password:";
    private static final char PASSWORD_CHARACTER = '*';
    private static final String LOGIN = "Login";
    private static final String REGISTER = "Register";
    private static final String CANCEL = "Cancel";
    private static final float ERROR_ANIMATION_STEP_TIME = 0.03f;

    private static String usernameCache = "";

    private Label errorMessage;
    private TextField usernameField;
    private TextField passwordField;
    private TextField passwordField2;
    private Type type;
    private Stage stage;

    public enum Type {
        LOGIN, REGISTER
    }

    public LoginDialog(Stage stage, final Type type, final LoginDialogListener listener) {
        super((type == Type.REGISTER) ? TITLE_REGISTER : TITLE_LOGIN, createSkin());
        this.stage = stage;
        this.type = type;
        float textFieldWidth = stage.getWidth() / 2;
        float textFieldHeight = 40;
        Label.LabelStyle errorSkin = new Label.LabelStyle(GameOfStarsStyles.Fonts.MEDIUM, GameOfStarsStyles.Colors.ERROR);
        errorSkin.background = GameOfStarsStyles.Drawables.ERROR_BACKGROUND;
        errorMessage = new Label("", errorSkin);
        errorMessage.setWrap(true);
        errorMessage.setVisible(false);
        getContentTable().add(errorMessage).fill();
        getContentTable().row();
        Label.LabelStyle hintSkin = new Label.LabelStyle(GameOfStarsStyles.Fonts.SMALL, Color.BLACK);
        TextField.TextFieldStyle fieldSkin = new TextField.TextFieldStyle(GameOfStarsStyles.Fonts.MEDIUM, Color.BLACK, GameOfStarsStyles.Drawables.TEXT_CURSOR, GameOfStarsStyles.Drawables.TEXT_SELECTION, GameOfStarsStyles.Drawables.TEXTFIELD_BACKGROUND);
        getContentTable().add(new Label(ENTER_USERNAME, hintSkin)).fill();
        getContentTable().row();
        usernameField = new TextField(usernameCache, fieldSkin);
        getContentTable().add(usernameField).width(textFieldWidth).height(textFieldHeight);
        getContentTable().row();
        getContentTable().add(new Label(ENTER_PASSWORD, hintSkin)).fill();
        getContentTable().row();
        passwordField = new TextField("", fieldSkin);
        passwordField.setPasswordMode(true);
        passwordField.setPasswordCharacter(PASSWORD_CHARACTER);
        getContentTable().add(passwordField).width(textFieldWidth).height(textFieldHeight);
        getContentTable().row();
        if (type == Type.REGISTER) {
            getContentTable().add(new Label(REPEAT_PASSWORD, hintSkin)).fill();
            getContentTable().row();
            passwordField2 = new TextField("", fieldSkin);
            passwordField2.setPasswordMode(true);
            passwordField2.setPasswordCharacter(PASSWORD_CHARACTER);
            getContentTable().add(passwordField2).width(textFieldWidth).height(textFieldHeight);
        }
        TextButton.TextButtonStyle btnSkin = new TextButton.TextButtonStyle(GameOfStarsStyles.Drawables.DIALOG_BUTTON, GameOfStarsStyles.Drawables.DIALOG_BUTTON_PRESSED, null, GameOfStarsStyles.Fonts.SMALL);
        TextButton login = new TextButton((type == Type.REGISTER) ? REGISTER : LOGIN, btnSkin);
        login.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                String username = usernameField.getText();
                usernameCache = username;
                String password = passwordField.getText();
                if (type == Type.REGISTER) {
                    String password2 = passwordField2.getText();
                    listener.onRegister(username, password, password2);
                } else {
                    listener.onLogin(username, password);
                }
                remove();
            }
        });
        getButtonTable().add(login).height(40).width(textFieldWidth / 3);
        TextButton cancel = new TextButton(CANCEL, btnSkin);
        cancel.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                remove();
            }
        });
        getButtonTable().add(cancel).height(40).width(textFieldWidth / 3);
        getButtonTable().pad(10, 0, 0, 0);
        pad(getStyle().titleFont.getLineHeight() + 20, 30, 15, 30);
    }

    public void setErrorMessage(String message) {
        errorMessage.setText(message);
        errorMessage.setVisible(true);
    }

    public void show() {
        show(stage);
        if (errorMessage.isVisible()) {
            Action moveRight = Actions.moveBy(15, 0, ERROR_ANIMATION_STEP_TIME, Interpolation.linear);
            Action moveLeft = Actions.moveBy(-15, 0, ERROR_ANIMATION_STEP_TIME, Interpolation.linear);
            Action moveBack = Actions.moveBy(0, 0, ERROR_ANIMATION_STEP_TIME, Interpolation.linear);
            Action move = Actions.sequence(moveLeft, moveBack, moveRight, moveBack);
            addAction(Actions.repeat(7, move));
        }
    }

    private static WindowStyle createSkin() {
        Window.WindowStyle skin = new Window.WindowStyle(GameOfStarsStyles.Fonts.BIG, Color.BLACK, GameOfStarsStyles.Drawables.DIALOG_BACKGROUND);
        return skin;
    }
}
