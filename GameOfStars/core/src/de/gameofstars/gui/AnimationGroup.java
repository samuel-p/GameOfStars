package de.gameofstars.gui;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by samuel on 22.02.15.
 */
public class AnimationGroup {
    private Map<Actor, Action> animations;
    private AnimationListener listener;
    private int finishedAnimationsCount;

    public AnimationGroup() {
        this.animations = new HashMap<Actor, Action>();
    }

    public void setListener(AnimationListener listener) {
        this.listener = listener;
    }

    public AnimationListener getListener() {
        return listener;
    }

    public void putAnimation(Actor actor, Action action) {
        animations.put(actor, action);
    }

    public void animate() {
        if (listener != null) {
            listener.onAnimationStarted();
        }
        for (Actor actor : animations.keySet()) {
            actor.addAction(Actions.sequence(animations.get(actor), Actions.run(new Runnable() {
                @Override
                public void run() {
                    finishedAnimationsCount++;
                    if (finishedAnimationsCount == animations.size()) {
                        if (listener != null) {
                            listener.onAnimationEnded();
                        }
                    }
                }
            })));
        }
    }

    public interface AnimationListener {
        void onAnimationStarted();

        void onAnimationEnded();
    }
}