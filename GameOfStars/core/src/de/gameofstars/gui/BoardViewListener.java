package de.gameofstars.gui;

/**
 * Created by samuel on 26.02.15.
 */
public interface BoardViewListener {
    void onGameFinished();
}