package de.gameofstars.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Created by samuel on 25.02.15.
 */
public class GameOfStarsStyles {
    static class Fonts {
        public static final BitmapFont SMALL;
        public static final BitmapFont MEDIUM;
        public static final BitmapFont BIG;

        static {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Roboto-Bold.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = calculateFontSize(12);
            SMALL = generator.generateFont(parameter);
            parameter.size = calculateFontSize(18);
            MEDIUM = generator.generateFont(parameter);
            parameter.size = calculateFontSize(24);
            BIG = generator.generateFont(parameter);
            Gdx.app.log("Density", "" + Gdx.graphics.getDensity());
        }

        private static int calculateFontSize(int size) {
            float fontSize = size * Gdx.graphics.getDensity() * 0.9f;
            int roundedSize = Math.round(fontSize);
            return Math.max(size, roundedSize);
        }
    }

    static class Drawables {
        public static final NinePatchDrawable LABEL_BACKGROUND;
        public static final NinePatchDrawable ERROR_BACKGROUND;
        public static final NinePatchDrawable BUTTON;
        public static final NinePatchDrawable BUTTON_PRESSED;
        public static final NinePatchDrawable BUTTON_LEFT;
        public static final NinePatchDrawable BUTTON_LEFT_PRESSED;
        public static final NinePatchDrawable BUTTON_RIGHT;
        public static final NinePatchDrawable BUTTON_RIGHT_PRESSED;
        public static final NinePatchDrawable BUTTON_TOP;
        public static final NinePatchDrawable BUTTON_TOP_PRESSED;
        public static final NinePatchDrawable BUTTON_BOTTOM;
        public static final NinePatchDrawable BUTTON_BOTTOM_PRESSED;
        public static final NinePatchDrawable BUTTON_CENTER_H;
        public static final NinePatchDrawable BUTTON_CENTER_H_PRESSED;
        public static final NinePatchDrawable BUTTON_CENTER_V;
        public static final NinePatchDrawable BUTTON_CENTER_V_PRESSED;
        public static final NinePatchDrawable BUTTON_NOTROUNDED_LEFT;
        public static final NinePatchDrawable BUTTON_NOTROUNDED_LEFT_PRESSED;
        public static final NinePatchDrawable BUTTON_NOTROUNDED_RIGHT;
        public static final NinePatchDrawable BUTTON_NOTROUNDED_RIGHT_PRESSED;
        public static final NinePatchDrawable DIALOG_BACKGROUND;
        public static final NinePatchDrawable DIALOG_BUTTON;
        public static final NinePatchDrawable DIALOG_BUTTON_PRESSED;
        public static final NinePatchDrawable TEXTFIELD_BACKGROUND;
        public static final NinePatchDrawable TEXT_CURSOR;
        public static final NinePatchDrawable TEXT_SELECTION;
        public static final NinePatchDrawable NO_SELECTION;
        public static final NinePatchDrawable TOAST_INFO_BACKGROUND;
        public static final NinePatchDrawable TOAST_ERROR_BACKGROUND;
        public static final NinePatchDrawable TOAST_SUCCESS_BACKGROUND;
        public static final NinePatchDrawable SELECTBOX_DROPDOWN_BACKGROUND;
        public static final NinePatchDrawable LIST_BACKGROUND_2;
        public static final NinePatchDrawable CUP_GOLD;
        public static final NinePatchDrawable CUP_SILVER;
        public static final NinePatchDrawable CUP_BRONZE;
        public static final NinePatchDrawable ARROW_LEFT;

        static {
            LABEL_BACKGROUND = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/label-background.png")), 10, 10, 10, 10));
            ERROR_BACKGROUND = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/error-background.png")), 7, 7, 7, 7));
            BUTTON = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-background.png")), 10, 10, 10, 10));
            BUTTON_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-pressed.png")), 10, 10, 10, 10));
            BUTTON_LEFT = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-left-background.png")), 10, 10, 10, 10));
            BUTTON_LEFT_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-left-pressed.png")), 10, 10, 10, 10));
            BUTTON_RIGHT = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-right-background.png")), 10, 10, 10, 10));
            BUTTON_RIGHT_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-right-pressed.png")), 10, 10, 10, 10));
            BUTTON_TOP = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-top-background.png")), 10, 10, 10, 10));
            BUTTON_TOP_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-top-pressed.png")), 10, 10, 10, 10));
            BUTTON_BOTTOM = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-bottom-background.png")), 10, 10, 10, 10));
            BUTTON_BOTTOM_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-bottom-pressed.png")), 10, 10, 10, 10));
            BUTTON_CENTER_H = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-center-horizontal-background.png")), 10, 10, 10, 10));
            BUTTON_CENTER_H_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-center-horizontal-pressed.png")), 10, 10, 10, 10));
            BUTTON_CENTER_V = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-center-vertical-background.png")), 10, 10, 10, 10));
            BUTTON_CENTER_V_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-center-vertical-pressed.png")), 10, 10, 10, 10));
            BUTTON_NOTROUNDED_LEFT = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-notrounded-left-background.png")), 10, 10, 10, 10));
            BUTTON_NOTROUNDED_LEFT_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-notrounded-left-pressed.png")), 10, 10, 10, 10));
            BUTTON_NOTROUNDED_RIGHT = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-notrounded-right-background.png")), 10, 10, 10, 10));
            BUTTON_NOTROUNDED_RIGHT_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/button-notrounded-right-pressed.png")), 10, 10, 10, 10));
            DIALOG_BACKGROUND = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/dialog-background.png")), 10, 10, 10, 10));
            DIALOG_BUTTON = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/dialog-button-background.png")), 5, 5, 5, 5));
            DIALOG_BUTTON_PRESSED = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/dialog-button-pressed.png")), 5, 5, 5, 5));
            TEXTFIELD_BACKGROUND = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/textfield-background.png")), 7, 7, 6, 6));
            TEXT_CURSOR = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/text-cursor.png"))));
            TEXT_SELECTION = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/text-selection.png"))));
            NO_SELECTION = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/no-selection.png"))));
            TOAST_INFO_BACKGROUND = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/toast-info-background.png")), 10, 10, 10, 10));
            TOAST_ERROR_BACKGROUND = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/toast-error-background.png")), 10, 10, 10, 10));
            TOAST_SUCCESS_BACKGROUND = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/toast-success-background.png")), 10, 10, 10, 10));
            SELECTBOX_DROPDOWN_BACKGROUND = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/selectbox-dropdown-background.png")), 1, 1, 1, 1));
            LIST_BACKGROUND_2 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/2nd-list-background.png"))));
            CUP_GOLD = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/cup-gold.png"))));
            CUP_SILVER = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/cup-silver.png"))));
            CUP_BRONZE = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/cup-bronze.png"))));
            ARROW_LEFT = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("imgs/arrow-left.png"))));
        }
    }

    static class Loading {
        public static final Drawable[] DRAWABLES;

        static {
            NinePatchDrawable load0 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("anim/loading/load_0.png"))));
            NinePatchDrawable load1 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("anim/loading/load_1.png"))));
            NinePatchDrawable load2 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("anim/loading/load_2.png"))));
            NinePatchDrawable load3 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("anim/loading/load_3.png"))));
            NinePatchDrawable load4 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("anim/loading/load_4.png"))));
            NinePatchDrawable load5 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("anim/loading/load_5.png"))));
            NinePatchDrawable load6 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("anim/loading/load_6.png"))));
            NinePatchDrawable load7 = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("anim/loading/load_7.png"))));
            DRAWABLES = new Drawable[] {load0, load1, load2, load3, load4, load5, load6, load7};
        }
    }

    static class Textures {
        public static final Texture STAR;
        public static final Texture LOGO;

        static {
            STAR = new Texture(Gdx.files.internal("imgs/star.png"));
            LOGO = new Texture(Gdx.files.internal("imgs/logo.png"));
        }
    }

    static class Colors {
        public static final Color INFO = new Color(0x003c82ff);
        public static final Color ERROR = new Color(0x940004ff);
        public static final Color SUCCESS = new Color(0x005c02ff);
    }
}