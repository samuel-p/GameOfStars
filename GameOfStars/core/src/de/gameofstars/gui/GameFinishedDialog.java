package de.gameofstars.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by samuel on 01.03.15.
 */
public class GameFinishedDialog extends Dialog {

    public static final String GAME_FINISHED = "Game finished:";
    public static final String SCORE = "Your Score: ";
    public static final String OK = "OK";

    public GameFinishedDialog(final GuiManager guiManager, int points) {
        super(GAME_FINISHED, createSkin());
        Label.LabelStyle labelStyle = new Label.LabelStyle(GameOfStarsStyles.Fonts.MEDIUM, Color.BLACK);
        getContentTable().add(new Label(SCORE + points, labelStyle));
        TextButton.TextButtonStyle btnStyle = new TextButton.TextButtonStyle(GameOfStarsStyles.Drawables.DIALOG_BUTTON, GameOfStarsStyles.Drawables.DIALOG_BUTTON_PRESSED, null, GameOfStarsStyles.Fonts.SMALL);
        TextButton button = new TextButton(OK, btnStyle);
        button.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                guiManager.setActiveScreen(ScreenIdentifier.STARTER);
            }
        });
        getButtonTable().add(button).height(40).width(80);
        pad(getStyle().titleFont.getLineHeight() + 20, 30, 15, 30);
    }

    private static WindowStyle createSkin() {
        Window.WindowStyle skin = new Window.WindowStyle(GameOfStarsStyles.Fonts.BIG, Color.BLACK, GameOfStarsStyles.Drawables.DIALOG_BACKGROUND);
        return skin;
    }

}
