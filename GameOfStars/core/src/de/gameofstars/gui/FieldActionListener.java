package de.gameofstars.gui;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import de.gameofstars.logic.Field;

/**
 * Created by samuel on 22.02.15.
 */
public class FieldActionListener extends DragListener {
    private Image image;
    private Field field;
    private FieldSwitchedListener listener;
    private Vector2 stageImagePosition;

    public FieldActionListener(Image image, Field field, FieldSwitchedListener listener) {
        this.image = image;
        this.field = field;
        this.listener = listener;
    }

    @Override
    public void drag(InputEvent event, float x, float y, int pointer) {
        // important for rotating images...
        Vector2 stagePoint = image.localToStageCoordinates(new Vector2(x, y));
        x = stagePoint.x - image.getX();
        y = stagePoint.y - image.getY();
        if (x > image.getWidth()) {
            invokeSwitch(field, field.getRight());
        } else if (x < 0) {
            invokeSwitch(field, field.getLeft());
        } else if (y > image.getHeight()) {
            invokeSwitch(field, field.getTop());
        } else if (y < 0) {
            invokeSwitch(field, field.getBottom());
        }
    }

    private void invokeSwitch(Field field1, Field field2) {
        if (field1 == null || field2 == null) {
            return;
        }
        cancel();
        if (listener != null) {
            listener.onFieldsSwitched(field1, field2);
        }
    }

    public interface FieldSwitchedListener {
        void onFieldsSwitched(Field field1, Field field2);
    }
}