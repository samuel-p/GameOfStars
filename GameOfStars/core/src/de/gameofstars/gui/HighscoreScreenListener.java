package de.gameofstars.gui;

import de.gameofstars.logic.GameLevel;
import de.gameofstars.logic.GameMode;

import java.util.Date;

/**
 * Created by samuel on 05.03.15.
 */
public interface HighscoreScreenListener {
    void onBackPressed();

    void onLoadHighscoreList(GameMode gameMode, GameLevel gameLevel, Date startDate, int startIndex, int pageSize);
}