package de.gameofstars.data;

import org.json.JSONObject;

import java.io.*;

/**
 * Created by samuel on 28.02.15.
 */
public class JsonStorage {
    private File file;
    private JSONObject storage;

    public JsonStorage(File file) {
        this.file = file;
        String fileContent = FileUtils.readFile(file);
        if (fileContent != null) {
            storage = new JSONObject(fileContent);
        } else {
            storage = new JSONObject();
        }
    }

    public void putString(String key, String value) {
        storage.put(key, value);
        FileUtils.writeFile(file, storage.toString());
    }

    public String getString(String key, String defaultValue) {
        if (storage.has(key)) {
            return storage.getString(key);
        }
        putString(key, defaultValue);
        return defaultValue;
    }
}