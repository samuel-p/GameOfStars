package de.gameofstars.data;

import de.gameofstars.encryption.Encryption;

import java.io.*;

/**
 * Created by samuel on 06.03.15.
 */
public class FileUtils {
    public static String readFile(File file) {
        try {
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            if (sb.length() > 0) {
                String content = sb.substring(0, sb.length() - 1);
                return Encryption.decrypt(content);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static void writeFile(File file, String content) {
        try {
            String encrypted = Encryption.encrypt(content);
            PrintWriter pw = new PrintWriter(file);
            pw.write(encrypted);
            pw.close();
        } catch (Exception e) {
        }
    }
}