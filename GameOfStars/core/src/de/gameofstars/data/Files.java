package de.gameofstars.data;

import com.badlogic.gdx.Gdx;

import java.io.File;

/**
 * Created by samuel on 01.03.15.
 */
public class Files {
    private static final String ROOT_PATH = ".gameofstars";

    public static File getFile(String name) {
        File root = getRootFolder();
        return new File(root, name);
    }

    public static File getRootFolder() {
        File root = Gdx.files.external(ROOT_PATH).file();
        if (!root.exists()) {
            root.mkdir();
        }
        return root;
    }
}