package de.gameofstars.data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by samuel on 01.03.15.
 */
public class JsonArrayStorage {
    private File file;
    private JSONArray storage;

    public JsonArrayStorage(File file) {
        this.file = file;
        String fileContent = FileUtils.readFile(file);
        if (fileContent != null) {
            storage = new JSONArray(fileContent);
        } else {
            storage = new JSONArray();
        }
    }

    public void add(JSONObject object) {
        storage.put(object);
        FileUtils.writeFile(file, storage.toString());
    }

    public List<JSONObject> get() {
        List<JSONObject> objects = new ArrayList<JSONObject>();
        for (int i = 0; i < storage.length(); i++) {
            objects.add(storage.getJSONObject(i));
        }
        return objects;
    }

    public void clear() {
        for (int i = 0; i < storage.length(); i++) {
            storage.remove(i);
        }
        FileUtils.writeFile(file, storage.toString());
    }
}