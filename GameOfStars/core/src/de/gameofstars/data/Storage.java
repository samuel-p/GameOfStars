package de.gameofstars.data;

import com.badlogic.gdx.Gdx;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by samuel on 28.02.15.
 */
public class Storage {
    public static final String STORAGE_NAME = "gameofstars";

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    private JsonStorage storage;

    public Storage() {
        File file = Files.getFile(STORAGE_NAME);
        storage = new JsonStorage(file);
    }

    public boolean isUserLoggedIn() {
        String username = getUsername();
        String password = getPassword();
        return username != null && password != null;
    }

    public String getUsername() {
        return storage.getString(USERNAME, null);
    }

    public void setUsername(String username) {
        storage.putString(USERNAME, username);
    }

    public String getPassword() {
        return storage.getString(PASSWORD, null);
    }

    public void setPassword(String password) {
        storage.putString(PASSWORD, password);
    }
}