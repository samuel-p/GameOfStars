package de.gameofstars.net;

/**
 * Created by samuel on 28.02.15.
 */
public class RequestKeys {
    public static final String ACTION = "action";
    public static final String STATE = "state";
    public static final String MESSAGE = "message";

    public class Action {
        public static final String LOGIN = "login";
        public static final String REGISTER = "register";
        public static final String COMMIT_SCORE = "commit-score";
        public static final String LIST_SCORE = "list-score";
    }

    public static class User {
        public static final String ID = "id";
        public static final String NAME = "username";
        public static final String PASSWORD = "password";
        public static final String PASSWORD2 = "password2";
        public static final String PASSWORD_LENGTH = "password-length";
    }

    public class Highscore {
        public static final String SCORE = "score";
        public static final String GAME_MODE = "game-mode";
        public static final String GAME_LEVEL = "game-level";
        public static final String DATETIME = "datetime";
        public static final String LIST_START_DATE = "list-start-date";
        public static final String LIST_START_INDEX = "list-start-index";
        public static final String LIST_SIZE = "list-size";
        public static final String LIST = "score-list";
        public static final String POSITION = "position";
    }
}