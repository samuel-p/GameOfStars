package de.gameofstars.logic;

import java.io.Serializable;

/**
 * Created by samuel on 25.02.15.
 */
public enum GameLevel {
    EASY, NORMAL, HARD
}