package de.gameofstars.encryption;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public abstract class Hash {
    public static String generateSHA512(String string) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(string.getBytes("UTF-8"), 0, string.length());
        byte[] sha1hash = md.digest();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sha1hash.length; i++) {
            int halfbyte = (sha1hash[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    sb.append((char) ('0' + halfbyte));
                else
                    sb.append((char) ('a' + (halfbyte - 10)));
                halfbyte = sha1hash[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return sb.toString();
    }

    public static String generateMD5(String string) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        messageDigest.update(string.getBytes());
        byte[] resultByte = messageDigest.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : resultByte)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }
}
