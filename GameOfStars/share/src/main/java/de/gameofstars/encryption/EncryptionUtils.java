package de.gameofstars.encryption;

import java.security.NoSuchAlgorithmException;

public class EncryptionUtils {
	private static final String numberListShuffled = "uyqVviONgFalEskXeopfrxTZhW4mcMLY2I1QUj95zGdC7DbSw6nAHR3t08PJKB";
	private static final String numberListSorted = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public static String parseNumber(long number) {
		if (number < 0)
			throw new NumberFormatException("The number can not be negative");
		StringBuilder sb = new StringBuilder();
		do {
			int index = (int) (number % numberListShuffled.length());
			number = number / numberListShuffled.length();
			sb.append(numberListShuffled.charAt(index));
		} while (number > 0);
		return sb.reverse().toString();
	}

	public static long printNumber(String string) {
		long number = 0;
		for (int i = string.length() - 1; i >= 0; i--) {
			int index = numberListShuffled.indexOf(String.valueOf(string
					.charAt(i)));
			number += index
					* Math.pow(numberListShuffled.length(), string.length() - i
							- 1);
		}
		return number;
	}

	public static char parseVersion(int version) {
		return numberListSorted.charAt(version);
	}

	public static int printVersion(char version) {
		return numberListSorted.indexOf(String.valueOf(version));
	}

	public static String getChecksum(String string) throws NoSuchAlgorithmException {
		StringBuilder sb = new StringBuilder();
		sb.append(parseNumber((long) Hash.generateMD5(string).hashCode()
				+ Integer.MAX_VALUE));
		int pos = 13;
		while (sb.length() < 6) {
			sb.append(numberListShuffled.charAt(pos));
			pos += 2;
		}
		return sb.reverse().toString();
	}
}