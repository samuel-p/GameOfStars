package de.gameofstars.encryption;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public abstract class KeyGenerator {
	private static final char[] allChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
			.toCharArray();

	public static String newKey(int length) {
		StringBuilder sb = new StringBuilder();
		Random r = new Random();
		for (int j = 0; j < length; j++) {
			sb.append(allChars[r.nextInt(allChars.length)]);
		}
		return sb.toString();
	}

	public static String shuffleKey(String key) {
		List<String> letters = Arrays.asList(key.split(""));
		Collections.shuffle(letters);
		StringBuilder sb = new StringBuilder(letters.size());
		for (String letter : letters) {
			sb.append(letter);
		}
		return sb.toString();
	}
}