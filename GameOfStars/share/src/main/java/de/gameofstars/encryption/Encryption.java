package de.gameofstars.encryption;

import java.security.InvalidAlgorithmParameterException;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {
    public static final String AES_OFB_NO_PADDING = "AES/OFB/NoPadding";
    public static final String AES = "AES";
    public static final String UTF_8 = "UTF-8";
    private static final int version = 1;
    private static final String KEY = "sTZuvAODoFbX1HHe6TAPHjRyArfeIvAuErSyjQHlpPZ1c4OigrQXYwSj1QYHCcS94S9xx0Ic7FhjGunGJ7pUNB05qeSgJ";
    private static final int KEY_LENGTH = 32;
    private static final int IVKEY_LENGTH = 16;
    public static final int RANDOM_MAX = 62;

    public static String encrypt(String string) throws Exception {
        String checksum = EncryptionUtils.getChecksum(string);
        Random r = new Random();
        int random = r.nextInt(RANDOM_MAX);
        String ivKey = KeyGenerator.newKey(IVKEY_LENGTH);
        String key1 = KEY.substring(random, random + KEY_LENGTH);
        String key2 = KeyGenerator.newKey(KEY_LENGTH);
        String encrypted = encrypt(encrypt(string, key1, ivKey), key2, ivKey);
        String basicBlock = encodeBasicBlock(random, encrypted.length(), checksum,
                key2, ivKey);
        StringBuilder sb = new StringBuilder();
        sb.append(EncryptionUtils.parseVersion(version));
        sb.append(EncryptionUtils.parseNumber(random));
        sb.append(encryptData(random, encrypted, basicBlock));
        return sb.toString();
    }

    public static String decrypt(String string) throws Exception {
        // version number for future encryption changes
        // InkomsEncryptionUtils.printVersion(string.charAt(0));
        int random = (int) EncryptionUtils.printNumber(String
                .valueOf(string.charAt(1)));
        String[] layer1 = decryptData(random, string.length() - 56,
                string.substring(2));
        String[] layer2 = decodeBasicBlock(random, string.length() - 56, layer1[0]);
        String key1 = KEY.substring(random, random + KEY_LENGTH);
        String decrypted = decrypt(
                decrypt(layer1[1], layer2[2], layer2[1]), key1, layer2[1]);
        if (EncryptionUtils.getChecksum(decrypted).equals(layer2[0])) {
            return decrypted;
        } else {
            throw new Exception();
        }
    }

    private static String encryptData(int random, String encryptedData,
                                      String basicBlock) {
        StringBuilder sb = new StringBuilder();
        int blockIndex = 0;
        int dataIndex = 0;
        for (int i = 0; i < basicBlock.length() + encryptedData.length(); i++) {
            float v1 = (random * (i + 1)) / (float) encryptedData.length();
            int r = (int) ((v1 - (int) v1) * random + 1);
            int mod = r % 20;
            boolean charPlaced = false;
            while (!charPlaced) {
                if (mod < 8 && blockIndex < basicBlock.length()) {
                    sb.append(basicBlock.charAt(blockIndex));
                    blockIndex++;
                    charPlaced = true;
                } else {
                    if (dataIndex < encryptedData.length()) {
                        sb.append(encryptedData.charAt(dataIndex));
                        dataIndex++;
                        charPlaced = true;
                    }
                }
                mod = 0;
            }
        }
        return sb.toString();
    }

    private static String[] decryptData(int random, int encryptedLength,
                                        String data) {
        StringBuilder sbBlock = new StringBuilder();
        StringBuilder sbEncrypted = new StringBuilder();
        for (int i = 0; i < data.length(); i++) {
            float v1 = (random * (i + 1)) / (float) encryptedLength;
            int r = (int) ((v1 - (int) v1) * random + 1);
            int mod = r % 20;
            boolean charPlaced = false;
            while (!charPlaced) {
                if (mod < 8 && sbBlock.length() < 6 + IVKEY_LENGTH + KEY_LENGTH) {
                    sbBlock.append(data.charAt(i));
                    charPlaced = true;
                } else {
                    if (sbEncrypted.length() < encryptedLength) {
                        sbEncrypted.append(data.charAt(i));
                        charPlaced = true;
                    }
                }
                mod = 0;
            }
        }
        return new String[]{sbBlock.toString(), sbEncrypted.toString()};
    }

    private static String encodeBasicBlock(int random, int messageLength,
                                           String checksum, String key, String ivKey) {
        StringBuilder sb = new StringBuilder();
        int checkIndex = 0;
        int ivIndex = 0;
        int key2Index = 0;
        for (int i = 0; i < checksum.length() + ivKey.length() + key.length(); i++) {
            float v1 = (random * (i + 1)) / (float) messageLength;
            int r = (int) ((v1 - (int) v1) * random + 1);
            int mod = r % 5;
            boolean charPlaced = false;
            while (!charPlaced) {
                switch (mod) {
                    case 0:
                        if (checkIndex < checksum.length()) {
                            sb.append(checksum.charAt(checkIndex));
                            checkIndex++;
                            charPlaced = true;
                            break;
                        }
                    case 1:
                        if (ivIndex < ivKey.length()) {
                            sb.append(ivKey.charAt(ivIndex));
                            ivIndex++;
                            charPlaced = true;
                            break;
                        }
                    case 2:
                    case 3:
                    case 4:
                        if (key2Index < key.length()) {
                            sb.append(key.charAt(key2Index));
                            key2Index++;
                            charPlaced = true;
                            break;
                        }
                        mod = 0;
                }
            }
        }
        return sb.toString();
    }

    private static String[] decodeBasicBlock(int random, int dataLength,
                                             String basicBlock) throws InvalidAlgorithmParameterException {
        if (basicBlock.length() != 6 + IVKEY_LENGTH + KEY_LENGTH) {
            throw new InvalidAlgorithmParameterException("BasicBlock is wrong!");
        }
        StringBuilder sbChecksum = new StringBuilder();
        StringBuilder sbIvKey = new StringBuilder();
        StringBuilder sbKey = new StringBuilder();
        for (int i = 0; i < basicBlock.length(); i++) {
            float v1 = (random * (i + 1)) / (float) dataLength;
            int r = (int) ((v1 - (int) v1) * random + 1);
            int mod = r % 5;
            boolean charPlaced = false;
            while (!charPlaced) {
                switch (mod) {
                    case 0:
                        if (sbChecksum.length() < 6) {
                            sbChecksum.append(basicBlock.charAt(i));
                            charPlaced = true;
                            break;
                        }
                    case 1:
                        if (sbIvKey.length() < IVKEY_LENGTH) {
                            sbIvKey.append(basicBlock.charAt(i));
                            charPlaced = true;
                            break;
                        }
                    case 2:
                    case 3:
                    case 4:
                        if (sbKey.length() < KEY_LENGTH) {
                            sbKey.append(basicBlock.charAt(i));
                            charPlaced = true;
                            break;
                        }
                        mod = 0;
                }
            }
        }
        return new String[]{sbChecksum.toString(), sbIvKey.toString(),
                sbKey.toString()};
    }

    private static String encrypt(String string, String key, String ivKey)
            throws Exception {
        Cipher cipher = Cipher.getInstance(AES_OFB_NO_PADDING);
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(UTF_8),
                AES);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey,
                new IvParameterSpec(ivKey.getBytes()));
        return Base64.encode(cipher.doFinal(string
                .getBytes(UTF_8)));
    }

    private static String decrypt(String string, String key, String ivKey)
            throws Exception {
        Cipher cipher = Cipher.getInstance(AES_OFB_NO_PADDING);
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(UTF_8),
                AES);
        cipher.init(Cipher.DECRYPT_MODE, secretKey,
                new IvParameterSpec(ivKey.getBytes(UTF_8)));
        String decrypted = new String(cipher.doFinal(Base64.decode(string)), UTF_8);
        return decrypted;
    }
}
